using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class earthuseSkill : MonoBehaviour
{
    [SerializeField] private float _damage;
    private GameObject _player;
    private bool explodeEnemyEnter;
    private SphereCollider expldeCollider;
    void Awake()
    {
        StartCoroutine(WaitCooldown());
        Destroy(gameObject,2f);
        _player = GameObject.Find("Player");
        explodeEnemyEnter = false;
        expldeCollider = GetComponent<SphereCollider>();
        expldeCollider.enabled = false;
    }

    private void FixedUpdate()
    {
        var position = _player.transform.position;
        transform.position = new Vector3(position.x,2.8f, position.z);
    }
    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (explodeEnemyEnter)
        {
            Damageable damageable = other.GetComponent<Damageable>();

            if (damageable != null && other.gameObject.CompareTag("Enemy"))
            {
                damageable.DoDamage(_damage);
            }
        }
    }
    IEnumerator WaitCooldown()
    {
        yield return new WaitForSeconds(1);
        expldeCollider.enabled = true;
        explodeEnemyEnter = true;
    }
}
