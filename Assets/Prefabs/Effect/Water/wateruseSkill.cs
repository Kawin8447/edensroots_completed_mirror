using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wateruseSkill : MonoBehaviour
{
    [SerializeField] private float _damage;
    [SerializeField] float _interval;
    [SerializeField] private int _heal;
    private GameObject _player;
    float _time;
    private bool enemyEnter;
    void Awake()
    {
        Destroy(gameObject,11);
        _time = 0f;
        _player = GameObject.Find("Player");
    }

    private void FixedUpdate()
    {
        var position = _player.transform.position;
        transform.position = new Vector3(position.x,1.8f, position.z);
    }
    // Update is called once per frame
    private void OnTriggerStay(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable != null && other.gameObject.CompareTag("Enemy"))
        {
            enemyEnter = true;
            _time += Time.deltaTime;
            while (_time >= _interval)
            {
                damageable.DoDamage(_damage);
                _time -= _interval;
            }
        }else if (damageable != null && other.gameObject.CompareTag("Player") && enemyEnter)
        { 
            PlayerManager _playerManager = other.GetComponent<PlayerManager>();
            if (_playerManager != null)
            { 
                _time += Time.deltaTime;
                while(_time >= _interval) 
                { 
                    _playerManager.HealPlayer(_heal);
                    _playerManager.curUltEnergy += 2;
                    _time -= _interval;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable != null && other.gameObject.CompareTag("Enemy"))
        {
            enemyEnter = false;
        }
    }
}
