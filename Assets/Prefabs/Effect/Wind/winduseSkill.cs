using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class winduseSkill : MonoBehaviour
{
    [SerializeField] private float _damage;
    [SerializeField] private int _heal;
     private GameObject _player;
     private GameObject _playerM;
    private PlayerManager _playerManager;
    void Awake()
    {
        Destroy(gameObject,0.3f);
        _playerM = GameObject.Find("PlayerManage(Clone)");
        _player = GameObject.Find("Player");
        _playerManager = _playerM.GetComponent<PlayerManager>();
    }

    private void FixedUpdate()
    {
        var position = _player.transform.position;
        transform.position = new Vector3(position.x,3.8f, position.z);
    }

    private void OnTriggerEnter(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();
        if (damageable != null && other.gameObject.CompareTag("Enemy"))
        {
            damageable.DoDamage(_damage);
            _playerManager.HealPlayer(_heal);
        }
    }
}
