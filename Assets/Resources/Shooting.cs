using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Shooting : MonoBehaviourPun
{
    public GameObject bulletPrefab;
    public Transform firePoint;
    public float bulletForce = 20f;

    void Update()
    {
        if (!photonView.IsMine) 
            return;
        
        shooter();
    }

    private void shooter()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            photonView.RPC("Shoot", RpcTarget.AllViaServer, firePoint.position, firePoint.rotation);
        }
    }

    [PunRPC]
    void Shoot(Vector3 position, Quaternion rotation, PhotonMessageInfo info)
    {
        if (!photonView.IsMine)
            return;

        GameObject bullet = PhotonNetwork.Instantiate(bulletPrefab.name, position, rotation);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
    }
}
