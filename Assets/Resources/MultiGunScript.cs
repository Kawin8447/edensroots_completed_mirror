using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MultiGunScript : MonoBehaviour
{
    [SerializeField] private Transform[] offsetPoint;
    [SerializeField] private GameObject[] ProjectilePrefab;
    [SerializeField] private float firingSpeed;
    [SerializeField] private float angle;
    private MultiPlayerManager _playerManager;
    void Awake()
    {
        _playerManager = GetComponentInParent<MultiPlayerManager>();
    }
    public void Shoot()
    {

        if (_playerManager._Element == MultiCurrentElement.None)
        {
            PhotonNetwork.Instantiate(ProjectilePrefab[0].name, offsetPoint[0].position, offsetPoint[0].rotation);
        } else if (_playerManager._Element == MultiCurrentElement.Water)
        {
            PhotonNetwork.Instantiate(ProjectilePrefab[1].name, offsetPoint[0].position, offsetPoint[0].rotation);
        } else if (_playerManager._Element == MultiCurrentElement.Earth)
        {
            PhotonNetwork.Instantiate(ProjectilePrefab[2].name, offsetPoint[0].position, offsetPoint[0].rotation);
            PhotonNetwork.Instantiate(ProjectilePrefab[2].name, offsetPoint[1].position, offsetPoint[1].rotation);
            PhotonNetwork.Instantiate(ProjectilePrefab[2].name, offsetPoint[2].position, offsetPoint[2].rotation);
        } else if (_playerManager._Element == MultiCurrentElement.Wind)
        {
            PhotonNetwork.Instantiate(ProjectilePrefab[3].name, offsetPoint[0].position, offsetPoint[0].rotation);
        }
    }
}
