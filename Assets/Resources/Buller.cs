using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Buller : MonoBehaviourPun
{
    public float lifeTime = 2f;
    private void Start()
    {
        Destroy(gameObject, lifeTime);
    }
    
    private void OnTriggerEnter(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();
        if (!other.CompareTag("Player"))
        {
            if (damageable != null && other.gameObject.CompareTag("Enemy"))
            {
                damageable.DoDamage(20);
                Destroy(gameObject);
            }
        }
    }
}
