using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiEarthSkill : MonoBehaviour
{
    
    [SerializeField] private float _damage;
    void Awake()
    {
        Destroy(gameObject,2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();
        if (damageable != null && other.gameObject.CompareTag("Enemy"))
        {
            damageable.DoDamage(_damage);
        }
    }
}
