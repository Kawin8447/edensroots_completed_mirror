using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Unity.Mathematics;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public Transform[] spawnPoints;
    public GameObject[] enemy;
    public float startTimeBtwSpawns;
    float timeBtwSpawns;
    public int maxEnemies;

    private void Start()
    {
        
    }

    private void Update()
    {
        GameObject[] AliveAi = GameObject.FindGameObjectsWithTag("Enemy");

        
        if (PhotonNetwork.IsMasterClient == false || PhotonNetwork.CurrentRoom.PlayerCount <1)
        {
            return;
        }
        
        if (timeBtwSpawns <= 0 && AliveAi.Length < 5)
        {
            for (int i = 0; i < maxEnemies; i++)
            {
                Vector3 spawnPosition = spawnPoints[Random.Range(0, spawnPoints.Length)].position;
                PhotonNetwork.Instantiate(enemy[Random.Range(0, enemy.Length)].name, spawnPosition, quaternion.identity);
            }

            if (maxEnemies < 5)
            {
                maxEnemies ++;
            }
            
            timeBtwSpawns = startTimeBtwSpawns;
        }
        else
        {
            timeBtwSpawns -= Time.deltaTime;
        }
    }
}
