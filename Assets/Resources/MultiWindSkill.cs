using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiWindSkill : MonoBehaviour
{
    [SerializeField] private float _damage;
    [SerializeField] private float _endDamage;
    [SerializeField] float _interval;
    float _time;
    void Awake()
    {
        Destroy(gameObject,6);
        _time = 0f;
        StartCoroutine(EndDamage());
    }
    
    private void OnTriggerStay(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable != null && other.gameObject.CompareTag("Enemy"))
        {
            _time += Time.deltaTime;
            while (_time >= _interval)
            {
                damageable.DoDamage(_damage);
                _time -= _interval;
            }
        }
    }

    private IEnumerator EndDamage()
    {
        yield return new WaitForSeconds(3);
        _damage += _endDamage;
    }
}
