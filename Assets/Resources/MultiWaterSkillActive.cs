using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiWaterSkillActive : MonoBehaviour
{

    [SerializeField] private float _damage;
    [SerializeField] private int _heal;
    [SerializeField] float _interval;
    float _time;
    
    void Awake()
    {
        Destroy(gameObject,11);
        _time = 0f;
    }
    
    private void OnTriggerStay(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable != null && other.gameObject.CompareTag("Enemy"))
        {
            _time += Time.deltaTime;
            while (_time >= _interval)
            {
                damageable.DoDamage(_damage);
                _time -= _interval;
            }
        }
        else if (damageable != null && other.gameObject.CompareTag("Player"))
        { 
            MultiPlayerManager _playerManager = other.GetComponent<MultiPlayerManager>();
            if (_playerManager != null)
            { 
                _time += Time.deltaTime;
                while(_time >= _interval) 
                { 
                    _playerManager.HealPlayer(_heal);
                    _time -= _interval; 
                }
            }
        }
    }
    
}