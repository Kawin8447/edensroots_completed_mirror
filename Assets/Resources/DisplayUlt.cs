using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public enum Elemem
{
    Water, 
    Earth, 
    Wind
};

public class DisplayUlt : MonoBehaviourPun
{
    private PhotonView PV;
    private MultiPlayerManager _playermanager;

    public Image ultBar;

    public Elemem _Element;

    private GameObject _player;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();

        switch (_Element)
        {
            case Elemem.Water:
                _player = GameObject.Find("MultiPlayerManageWater(Clone)");
                _playermanager = _player.GetComponent<MultiPlayerManager>();
                break;
            case Elemem.Earth:
                _player = GameObject.Find("MultiPlayerManageEarth(Clone)");
                _playermanager = _player.GetComponent<MultiPlayerManager>();
                break;
            case Elemem.Wind:
                _player = GameObject.Find("MultiPlayerManageWind(Clone)");
                _playermanager = _player.GetComponent<MultiPlayerManager>();
                break;
        }
        
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!PV.IsMine)
        {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        ultBar.fillAmount = _playermanager.curUltEnergy / _playermanager.MaxultEnergy;
    }
}
