using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using Photon.Pun;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public enum MultiStatus
{
    Idle,
    Move,
    Stunt
};

public enum MultiCurrentElement
{
    None,
    Earth,
    Water,
    Wind
};

public class MultiPlayerManager : MonoBehaviour, Damageable
{
    [Header("PlayerStatus")]
    public MultiStatus _Status;
    public MultiCurrentElement _Element;
    public float invincibleLength;
    [SerializeField] private GameObject player;
    public Vector3 currentPosition;

    [Header("PlayerData")]
    public ObjectStat[] data;

    [Header("Ult Object")]
    public GameObject[] ult;

    [Header("Skill Object")]
    public GameObject[] Skill;

    [Header("Stat")]
    public float playerHp;
    public float maxPlayerHp;
    public float playerMana;
    public float maxPlayerMana;
    public int damage;
    public int speed;
    public float fireRate;
    public float curUltEnergy;
    public float MaxultEnergy;
    public float cooldownTime;
    public int DashRange;
    public int DashCooldown;

    [Header("Material")]
    [SerializeField] private GameObject[] tranformEffect;
    public static bool gameIsPaused;

    //Value
    public float valueLoss = 0.5f;
    public float invincibillityCounter;
    public float cooldownDuration;
    public bool isCooldown;
    
    PhotonView PV;
    
    public Image HealthBar;
    public TextMeshProUGUI HealthText;

    private GameObject Gmanage;
    private GlobalMamage globalMamage;

    public GameObject model;
    private void Awake()
    {
        PV = GetComponent<PhotonView>();
        
        Gmanage = GameObject.Find("GloabalManager");
        globalMamage = Gmanage.GetComponent<GlobalMamage>();
    }

    private void Start()
    {
        playerHp = 150;
    }

    void Update()
    {
        tranformEffect[0].transform.position = player.transform.position;
        tranformEffect[1].transform.position = player.transform.position;
        tranformEffect[2].transform.position = player.transform.position;
        tranformEffect[3].transform.position = player.transform.position;

        if (player != null)
        {
            currentPosition = player.transform.position;
            transform.position = player.transform.position;
        }
        else
        {
            Debug.Log("Assign Player Prefab");
        }
        if (isCooldown)
        {
            cooldownDuration -= Time.deltaTime;
            if (cooldownDuration <= 0f)
            {
                isCooldown = false;
                Debug.Log("Ability is ready!");
            }
        }

        if (invincibillityCounter > 0)
        {
            invincibillityCounter -= Time.deltaTime;
        }

        //DecayHp if overhealth
        if (playerHp > maxPlayerHp)
        {
                playerHp -= valueLoss * Time.deltaTime;
        }
        else
        {
            playerHp = Mathf.Round(playerHp * 10.0f) * 0.1f;
        }

        if (curUltEnergy > MaxultEnergy)
        {
            curUltEnergy = MaxultEnergy;
        }
        else if (curUltEnergy < 0)
        {
            curUltEnergy = 0;
        }
        switch (_Status)
        {
            case MultiStatus.Idle:
                break;
            case MultiStatus.Move:
                break;
            case MultiStatus.Stunt:
                break;
        }
        switch (_Element)
        {
            case MultiCurrentElement.None:
                maxPlayerHp = data[0].MaxHp;
                maxPlayerMana = data[0].MaxMana;
                damage = data[0].Damage;
                speed = data[0].Speed;
                fireRate = data[0].FireSpeed;
                MaxultEnergy = data[0].Energy;
                cooldownTime = data[0].SkillCooldown;
                playerMana = maxPlayerMana; //inf mana
                break;
            case MultiCurrentElement.Water:
                maxPlayerHp = 100;
                maxPlayerMana = 100;
                damage = 20;
                speed = 8;
                fireRate = 0.2f;
                MaxultEnergy = 100;
                cooldownTime = 15;
                playerMana = maxPlayerMana; //inf mana
                break;
            case MultiCurrentElement.Earth:
                maxPlayerHp = 150;
                maxPlayerMana = 70;
                damage = 40;
                speed = 7;
                fireRate = 0.6f;
                MaxultEnergy = 50;
                cooldownTime = 15;
                playerMana = maxPlayerMana; //inf mana
                break;
            case MultiCurrentElement.Wind:
                maxPlayerHp = 60;
                maxPlayerMana = 150;
                damage = 35;
                speed = 12;
                fireRate = 0.4f;
                MaxultEnergy = 70;
                cooldownTime = 3;
                playerMana = maxPlayerMana; //inf mana
                break;
        }
        PV.RPC("UpdateHealthBar", RpcTarget.AllBuffered, playerHp);
    }
    public void ActivateAbility()
    {
        if (!isCooldown)
        {
            isCooldown = true;
            cooldownDuration = cooldownTime;
            Debug.Log("Ability activated!");
        }
        else
        {
            Debug.Log("Ability is on cooldown!");
        }
    }
    public void DoDamage(float damage)
    {
        if (playerHp > 0 && invincibillityCounter <= 0)
        {
            HitPlayer(damage, Vector3.zero);
            StartCoroutine("Invincible");
        }
    }

    public IEnumerable Invincible()
    {
        player.GetComponent<Renderer>();
        Physics.IgnoreLayerCollision(7, 7, true);
        yield return new WaitForSeconds(invincibleLength);
        Physics.IgnoreLayerCollision(7, 7, false);
    }

    public void HitPlayer(float damage, Vector3 dir)
    {
        if (invincibillityCounter <= 0)
        {
            PV.RPC("DealingDamage", RpcTarget.AllBuffered, damage);
            invincibillityCounter = invincibleLength;
        }
    }

    public void HealPlayer(int heal)
    {
        PV.RPC("Healing", RpcTarget.AllBuffered, heal);
    }

    public void TranformElement(string Element)
    {
        if (Element == "None")
        {
            Instantiate(tranformEffect[0], player.transform.position, tranformEffect[0].transform.rotation);
        }
        else if (Element == "Water")
        {
            Instantiate(tranformEffect[1], player.transform.position, tranformEffect[1].transform.rotation);
        }
        else if (Element == "Earth")
        {
            Instantiate(tranformEffect[2], player.transform.position, tranformEffect[2].transform.rotation);
        }
        else if (Element == "Wind")
        {
            Instantiate(tranformEffect[3], player.transform.position, Quaternion.identity);
        }
    }
    [PunRPC]
    private void UpdateHealthBar(float health)
    {
        float currentHealth = health;
        HealthBar.fillAmount = currentHealth / maxPlayerHp;
        HealthText.text = Mathf.Round(currentHealth).ToString();
    }
    [PunRPC]
    private void DealingDamage(float Damage)
    {
        playerHp -= Damage;
        
        if (playerHp <= 0)
        {
            Debug.LogWarning("Deaddddddddddddddd");
            if (PV.IsMine)
            {
                PV.RPC("DisablePlayer", RpcTarget.All);
                GameOver();
            }
        }
    }

    [PunRPC]
    private void Healing(int healer)
    {
        playerHp += healer;
    }
    [PunRPC]
    private void DisablePlayer()
    {
        Destroy(gameObject);
    }

    private void GameOver()
    {
        globalMamage.gameover();
    }
}
