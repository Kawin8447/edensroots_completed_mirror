using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using Unity.Mathematics;

public class RangeMultiAi : MonoBehaviourPunCallbacks, Damageable
{
    public string PlayerObject;

    public NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    public float Health;
    private float Maxhealth = 100;
    private float currentHealth;

    public Image HealthBar;
    public TextMeshProUGUI HealthText;

    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    //Attacking
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public GameObject Projectiles;

    //States
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    public GameObject shooterPoint;
    
    [SerializeField]private Animator enemyAnim;

    private void Awake()
    {
        player = GameObject.FindWithTag(PlayerObject).transform;
        agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        Maxhealth *= 1 + (GlobalMamage.instance.score / 20f);
        Health = Maxhealth;
    }

    private void Update()
    {
        //Check for sight and attack range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);


        if (Health > 0 && !playerInSightRange && !playerInAttackRange) Patroling();
        if (Health > 0 && playerInSightRange && !playerInAttackRange) ChasePlayer();
        if (Health > 0 && playerInAttackRange && playerInSightRange) AttackPlayer();
        
        //photonView.RPC("CalculateHealthBar", RpcTarget.AllBuffered, Health);
        //for Hp bar
        photonView.RPC("UpdateHealthBar", RpcTarget.AllBuffered, Health, Maxhealth);
    }

    private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        transform.LookAt(player);
        agent.SetDestination(player.position);
    }

    private void AttackPlayer()
    {
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);

        transform.LookAt(player);

        if (!alreadyAttacked)
        {
            
            ////Attack code
            Rigidbody rb = Instantiate(Projectiles, shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 40f, ForceMode.Impulse);

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
            
            enemyAnim.SetTrigger("Attack");
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
    

    public void DoDamage(float damage)
    {
        photonView.RPC("SetupDamage", RpcTarget.AllViaServer, damage);
        //SetupDamage(damage);
    }
    [PunRPC]
    private void SetupDamage(float Damage)
    {
        Health -= Damage;

        //Debug.Log("Ai take damage by bullet HP = " + Health);
        if (Health <= 0)
        {
            agent.enabled = false;
            photonView.RPC("AddScore", RpcTarget.All, 1f);
            Destroy(gameObject);
        }
    }

    [PunRPC]
    private void UpdateHealthBar(float health, float maxHealthPlus)
    {
        Maxhealth = maxHealthPlus;
        currentHealth = health;
        HealthBar.fillAmount = currentHealth / Maxhealth;
        HealthText.text = Mathf.Round(currentHealth).ToString();
    }
    [PunRPC]
    private void AddScore(float score)
    {
        GlobalMamage.instance.score += score;
    }
}
