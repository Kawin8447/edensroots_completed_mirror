using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using Unity.Mathematics;

public class MeleeMultiAi : MonoBehaviourPunCallbacks, Damageable
{
    public string PlayerObject;

    public NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    public float Health;
    private float Maxhealth = 150;
    private float currentHealth;

    public Image HealthBar;
    public TextMeshProUGUI HealthText;

    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    //Attacking
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public GameObject Projectiles;

    //States
    public float sightRange, attackRange, SkillRange;
    public bool playerInSightRange, playerInAttackRange, playerInSkillRange;

    public GameObject shooterPoint;

    [Header("Dash")]
    public bool CanDash, IsDashing;
    public float DashCooldown = 5;

    float distanceToPlayer;
    
    public ParticleSystem ChargeEffect;

    public GameObject ChargeAttackHitBox;
    
    [SerializeField]private Animator enemyAnim;
    private bool isCharging;

    private void Awake()
    {
        player = GameObject.Find(PlayerObject).transform;
        agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        CanDash = true;
        IsDashing = false;
        Maxhealth *= 1 + (GlobalMamage.instance.score / 20f);
        Health = Maxhealth;
    }

    private void Update()
    {
        //Check for sight and attack range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);
        playerInSkillRange = Physics.CheckSphere(transform.position, SkillRange, whatIsPlayer);


        if (Health > 0 && !playerInSightRange && !playerInAttackRange && !IsDashing) Patroling();
        if (Health > 0 && playerInSightRange && !playerInAttackRange && !IsDashing) ChasePlayer();
        if (Health > 0 && playerInAttackRange && playerInSightRange && !IsDashing) AttackPlayer();

        if (Health > 0 && playerInSightRange && playerInSkillRange && !playerInAttackRange && !IsDashing)
        {
            {
                System.Action[] skills = new System.Action[] { Skill_Dash };

                int randomIndex = Random.Range(0, skills.Length);

                skills[randomIndex]();
            }
        }

        if (IsDashing) Dashing();
        
        //photonView.RPC("CalculateHealthBar", RpcTarget.AllBuffered, Health);
        //for Hp bar
        photonView.RPC("UpdateHealthBar", RpcTarget.AllBuffered, Health, Maxhealth);
    }

    private void Patroling()
    {
        enemyAnim.SetBool("IsMove",true);
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        if (isCharging)
        {
            enemyAnim.SetBool("IsMove",false);
        }
        else
        {
            enemyAnim.SetBool("IsMove",true);
            enemyAnim.SetBool("Charge Attack",false);
        }
        
        transform.LookAt(player);
        agent.SetDestination(player.position);
    }

    private void AttackPlayer()
    {
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);
        enemyAnim.SetTrigger("Attack");
        
        transform.LookAt(player);

        if (!alreadyAttacked)
        {
            
            ////Attack code
            Rigidbody rb = Instantiate(Projectiles, shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 20f, ForceMode.Impulse);

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, SkillRange);
    }
    

    public void DoDamage(float damage)
    {
        photonView.RPC("SetupDamage", RpcTarget.AllViaServer, damage);
        //SetupDamage(damage);
    }
    [PunRPC]
    private void SetupDamage(float Damage)
    {
        Health -= Damage;

        //Debug.Log("Ai take damage by bullet HP = " + Health);
        if (Health <= 0)
        {
            agent.enabled = false;
            photonView.RPC("AddScore", RpcTarget.All, 1f);
            Destroy(gameObject);
        }
    }

    private void Skill_Dash()
    {
        if (CanDash == true)
        {
            StartCoroutine(Dash());
        }
    }
    IEnumerator Dash()
    {
        //start stay
        ChargeEffect.Play();
        isCharging = true;
        enemyAnim.SetBool("isMove",false);
        enemyAnim.SetBool("Charge Start",true);
        CreateChargeAttackHitBox();
        CanDash = false;
        agent.isStopped = true;

        //stay for 1s
        yield return new WaitForSeconds(1f);

        //start dash
        ChargeEffect.Stop();
        isCharging = false;
        enemyAnim.SetBool("Charge Start",false);
        enemyAnim.SetBool("Charge Attack",true);
        IsDashing = true;
        transform.LookAt(transform.position + transform.forward);
        distanceToPlayer = Vector3.Distance(transform.position, player.position);
        //Debug.LogWarning("distanceToPlayer = " + distanceToPlayer);

        if (distanceToPlayer > 12) distanceToPlayer = 12;

        //dash for Range sec
        yield return new WaitForSeconds(distanceToPlayer / 10);

        //start reset
        HideChargeAttackHitBox();
        IsDashing = false;
        agent.isStopped = false;
        agent.acceleration = 0;

        yield return new WaitForSeconds(.1f);
        agent.SetDestination(transform.position);
        agent.acceleration = 120;

        //wait for cooldown
        yield return new WaitForSeconds(DashCooldown);

        //done reset
        CanDash = true;
    }

    private void Dashing()
    {
        agent.Move(transform.forward * (distanceToPlayer * 2) * Time.deltaTime);
    }
    private void CreateChargeAttackHitBox()
    {
        ChargeAttackHitBox.SetActive(true);
    }
    private void HideChargeAttackHitBox()
    {
        ChargeAttackHitBox.SetActive(false);
    }

    [PunRPC]
    private void UpdateHealthBar(float health, float maxHealthPlus)
    {
        Maxhealth = maxHealthPlus;
        currentHealth = health;
        HealthBar.fillAmount = currentHealth / Maxhealth;
        HealthText.text = Mathf.Round(currentHealth).ToString();
    }
    [PunRPC]
    private void AddScore(float score)
    {
        GlobalMamage.instance.score += score;
    }
}
