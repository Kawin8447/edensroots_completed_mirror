using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using Photon.Pun;
using UnityEngine.UI;
using Slider = UnityEngine.UI.Slider;

public class PlayerController : MonoBehaviourPunCallbacks
{
    public float rotationSpeed = 5f;

    PhotonView PV;

    private Vector2 _mousepos;
    [SerializeField] private Camera camera1;

    public string playerName;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    private void Start()
    {
        if (!PV.IsMine)
        {
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(GetComponentInChildren<CinemachineFreeLook>().gameObject);
        }
    }

    void Update()
    {

        if (!PV.IsMine)
            return;

        Moving();
        RotateWithMouse();
    }
    void Moving()
    {
        float x = Input.GetAxisRaw("Horizontal") * 10f * Time.deltaTime;
        float z = Input.GetAxisRaw("Vertical") * 10f * Time.deltaTime;
        transform.Translate(x, 0, z);
    }

    private void RotateWithMouse()
    {
        Ray ray = camera1.ScreenPointToRay(_mousepos);
        if (Physics.Raycast(ray, out RaycastHit hitInfo, maxDistance: 500f))
        {
            var target = hitInfo.point;
            target.y = transform.position.y;
            transform.LookAt(target);
        }
    }
    public void MousePosition(InputAction.CallbackContext context)
    {
        _mousepos = context.action.ReadValue<Vector2>();
    }
}