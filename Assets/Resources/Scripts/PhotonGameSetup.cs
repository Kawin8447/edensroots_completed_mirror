using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonGameSetup : MonoBehaviour
{
    public static PhotonGameSetup PGS;

    public Camera MyCamera;

    public void Awake()
    {
        PhotonGameSetup.PGS = this;
    }
}
