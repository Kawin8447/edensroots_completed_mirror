using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;
using System;
using Photon.Realtime;
using Unity.Mathematics;
using System.Linq;

public enum PlayerClass
{
    Water,
    Earth,
    Wind
}

public class ClassSelectionManage : MonoBehaviourPunCallbacks
{
    public TMP_Dropdown classDropdown;
    public static PlayerClass SelectedClass1 { get; private set; }
    public static PlayerClass SelectedClass2 { get; private set; }
    public static PlayerClass SelectedClass3 { get; private set; }

    private static ClassSelectionManage instance;

    public TextMeshProUGUI ElemToShow1;
    public TextMeshProUGUI ElemToShow2;
    public TextMeshProUGUI ElemToShow3;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        PopulateDropdown();
        classDropdown.onValueChanged.AddListener(delegate { OnClassDropdownValueChanged(); });
    }

    private void PopulateDropdown()
    {
        if (classDropdown != null)
            classDropdown.ClearOptions();
        classDropdown.AddOptions(new List<string>(Enum.GetNames(typeof(PlayerClass))));

        classDropdown.value = (int)SelectedClass1;
        classDropdown.value = (int)SelectedClass2;
        classDropdown.value = (int)SelectedClass3;
    }

    public void OnClassDropdownValueChanged()
    {
        SelectedClass1 = (PlayerClass)classDropdown.value;
        SelectedClass2 = (PlayerClass)classDropdown.value;
        SelectedClass3 = (PlayerClass)classDropdown.value;
        //Debug.Log("Selected class: " + SelectedClass1.ToString());
        
        if (PhotonNetwork.NickName == "Player 1")
        {
            photonView.RPC("SyncSelectedClass", RpcTarget.AllBuffered, (int)SelectedClass1, SelectedClass1.ToString());
        }
        else if (PhotonNetwork.NickName == "Player 2")
        {
            photonView.RPC("SyncSelectedClass2", RpcTarget.AllBuffered, (int)SelectedClass2, SelectedClass2.ToString());
        }
        else if (PhotonNetwork.NickName == "Player 3")
        {
            photonView.RPC("SyncSelectedClass3", RpcTarget.AllBuffered, (int)SelectedClass3, SelectedClass3.ToString());
        }
    }

    [PunRPC]
    private void SyncSelectedClass(int selectedClassValue, string Elem)
    {
        SelectedClass1 = (PlayerClass)selectedClassValue;
        if (Elem == "Water")
        {
            ElemToShow1.text = "P1 Element: <color=blue>Water</color>";
        }
        else if (Elem == "Earth")
        {
            ElemToShow1.text = "P1 Element: <color=orange>Earth</color>";
        }
        else if (Elem == "Wind")
        {
            ElemToShow1.text = "P1 Element: <color=green>Wind</color>";
        }
    }

    [PunRPC]
    private void SyncSelectedClass2(int selectedClassValue, string Elem)
    {
        SelectedClass2 = (PlayerClass)selectedClassValue;
        if (Elem == "Water")
        {
            ElemToShow2.text = "P2 Element: <color=blue>Water</color>";
        }
        else if (Elem == "Earth")
        {
            ElemToShow2.text = "P2 Element: <color=orange>Earth</color>";
        }
        else if (Elem == "Wind")
        {
            ElemToShow2.text = "P2 Element: <color=green>Wind</color>";
        }
    }

    [PunRPC]
    private void SyncSelectedClass3(int selectedClassValue, string Elem)
    {
        SelectedClass3 = (PlayerClass)selectedClassValue;
        if (Elem == "Water")
        {
            ElemToShow3.text = "P3 Element: <color=blue>Water</color>";
        }
        else if (Elem == "Earth")
        {
            ElemToShow3.text = "P3 Element: <color=orange>Earth</color>";
        }
        else if (Elem == "Wind")
        {
            ElemToShow3.text = "P3 Element: <color=green>Wind</color>";
        }
    }
}