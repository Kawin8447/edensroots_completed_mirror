using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using Unity.VisualScripting;

public class PunPlayerManager : MonoBehaviourPunCallbacks
{
    PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (PV.IsMine)
        {
            CreateController();
        }
    }

    void CreateController()
    {
        PhotonNetwork.Instantiate((Path.Combine("Prefabs", "PlayerController")), Vector3.zero, Quaternion.identity);
        
    }
}
