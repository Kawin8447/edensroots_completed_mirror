using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class RoomManager : MonoBehaviourPunCallbacks
{
    public static RoomManager Instance;

    public GameObject[] playerPrefabs;

    private void Awake()
    {
        if (Instance)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {
        if (scene.buildIndex == 2) // in the game scene
        {
            GameObject playerSpawnpoint = GameObject.Find("SpawnPoint");

            /* PhotonNetwork.Instantiate(Path.Combine("PlayerController"), playerSpawnpoint.transform.position, Quaternion.identity); */

            if (PhotonNetwork.NickName == "Player 1")
            {
                GameObject playerPrefab = playerPrefabs[(int)ClassSelectionManage.SelectedClass1];
                PhotonNetwork.Instantiate(playerPrefab.name, playerSpawnpoint.transform.position, Quaternion.identity);
            }
            if (PhotonNetwork.NickName == "Player 2")
            {
                GameObject playerPrefab = playerPrefabs[(int)ClassSelectionManage.SelectedClass2];
                PhotonNetwork.Instantiate(playerPrefab.name, playerSpawnpoint.transform.position, Quaternion.identity);
            }
            if (PhotonNetwork.NickName == "Player 3")
            {
                GameObject playerPrefab = playerPrefabs[(int)ClassSelectionManage.SelectedClass3];
                PhotonNetwork.Instantiate(playerPrefab.name, playerSpawnpoint.transform.position, Quaternion.identity);
            }
        }
    }
}
