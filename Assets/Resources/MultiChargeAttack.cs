using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MultiChargeAttack : MonoBehaviour
{
    public float damageAmount = 20;

    private void OnTriggerEnter(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable != null && !other.gameObject.CompareTag("Enemy"))
        {
            damageable.DoDamage(damageAmount);
        }
    }
}
