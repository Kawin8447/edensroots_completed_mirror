using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GlobalMamage : MonoBehaviour
{
    public static GlobalMamage instance;
    public float score;
    
    [Header("Timer Text")]
    public TextMeshProUGUI timerText;
    private float elapsedTime = 0f;
    private int minutes;
    private int seconds;
    
    PhotonView PV;
    
    private int lastminutes;
    private int lastseconds;

    public GameObject Gameoverpanel;
    public TextMeshProUGUI lasttime;

    private void Awake()
    {
        instance = this;
        
        PV = GetComponent<PhotonView>();
    }
    private void Update()
    {
        elapsedTime += Time.deltaTime;
        minutes = Mathf.FloorToInt(elapsedTime / 60f);
        seconds = Mathf.FloorToInt(elapsedTime % 60f);
        
        timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    public void gameover()
    {
        Gameoverpanel.gameObject.SetActive(true);
        
        lastseconds = seconds;
        lastminutes = minutes;
        lasttime.text = string.Format("{0:00}:{1:00}", lastminutes, lastseconds);
    }

    public void Disc()
    {
        if (!PV.IsMine) return;
        
        PhotonNetwork.Disconnect();
        SceneManager.LoadScene(sceneBuildIndex: 0);
    }
}
