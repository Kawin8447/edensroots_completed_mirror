using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using Cinemachine;

[RequireComponent(typeof(CharacterController))]
public class MultiPlayerControllerSys : MonoBehaviour
{
    private Animator _pAnimation;
    private float _speedVelocity;
    private Vector3 _lastPos;
    public Vector2 _input;
    private Vector2 _mousepos;
    private CharacterController _characterController;
    private Vector3 _direction;
    private float speed;
    private MultiPlayerManager _playerManager;
    private bool allowFire;
    private bool CancleFire;
    private bool fireOnCooldown;
    private TrailRenderer _trailRenderer;
    public bool onDash;
    [SerializeField] private Camera camera1;
    private float verticalVelocity;
    private float gravityValue = 9.81f;
    [SerializeField] private Material[] trailMaterials;

    [Header("Raycast Interaction Element")]
    private GameObject lastHitObject;
    private Transform childObject;

    private PhotonView Pv;
    public GameObject CamObject;
    
    private MultiGunScript _gunScript;
    
    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _playerManager = GetComponentInParent<MultiPlayerManager>();
        _trailRenderer = GetComponent<TrailRenderer>();
        _pAnimation = GetComponentInChildren<Animator>();
        Pv = GetComponent<PhotonView>();
        _gunScript = GetComponentInChildren<MultiGunScript>();
    }

    private void Start()
    {
        if (!Pv.IsMine)
        {
            Destroy(CamObject);
        }
    }

    private void Update()
    {
        switch (_playerManager._Element)
        {
            case MultiCurrentElement.None:
                _trailRenderer.material = new Material(trailMaterials[0]);
                break;
            case MultiCurrentElement.Water:
                _trailRenderer.material = new Material(trailMaterials[1]);
                break;
            case MultiCurrentElement.Earth:
                _trailRenderer.material = new Material(trailMaterials[2]);
                break;
            case MultiCurrentElement.Wind:
                _trailRenderer.material = new Material(trailMaterials[3]);
                break;
        }
        
        if (!Pv.IsMine)
            return;
        
        bool groundedPlayer = _characterController.isGrounded;
        speed = _playerManager.speed;
        if (groundedPlayer && verticalVelocity < 0)
        {
            // hit ground
            verticalVelocity = 0f;
        }
        verticalVelocity -= gravityValue * Time.deltaTime;
        _direction.y = verticalVelocity;
        _characterController.Move(_direction * speed * Time.deltaTime);
        
        if (_playerManager._Status != MultiStatus.Stunt && Time.timeScale != 0)
        {
            RotateWithMouse();
        }
        if (allowFire == true && Time.timeScale != 0)
        {
            Shooting();
        }
        _speedVelocity = ((transform.position - _lastPos).magnitude) / Time.deltaTime;
        _lastPos = transform.position;
        _pAnimation.SetFloat("Speed",_speedVelocity);
    }

    private void RotateWithMouse()
    {
        if (camera1 == null) return; // เพิ่มเงื่อนไขตรวจสอบว่า camera1 ไม่เป็น null

        Ray ray = camera1.ScreenPointToRay(_mousepos);
        if (Physics.Raycast(ray, out RaycastHit hitInfo, maxDistance: 500f))
        {
            var target = hitInfo.point;
            target.y = transform.position.y;
            transform.LookAt(target);
        }

        if (Physics.Raycast(ray, out RaycastHit hitWater) && hitWater.collider.gameObject.name == "ReWater(Clone)")
        {
            Debug.LogWarning("This is Water");
            hitWater.collider.gameObject.GetComponent<Remana>().ShowHint();
            if (Input.GetKeyDown(KeyCode.F))
            {
                hitWater.collider.gameObject.GetComponent<Remana>().CallPickup();
            }
        }

        if (Physics.Raycast(ray, out RaycastHit hitEarth) && hitEarth.collider.gameObject.name == "ReEarth(Clone)")
        {
            Debug.LogWarning("This is Earth");
            hitEarth.collider.gameObject.GetComponent<Remana>().ShowHint();
            if (Input.GetKeyDown(KeyCode.F))
            {
                hitWater.collider.gameObject.GetComponent<Remana>().CallPickup();
            }
        }


        if (Physics.Raycast(ray, out RaycastHit hitWind) && hitWind.collider.gameObject.name == "ReWind(Clone)")
        {
            Debug.LogWarning("This is Wind");
            hitWind.collider.gameObject.GetComponent<Remana>().ShowHint();
            if (Input.GetKeyDown(KeyCode.F))
            {
                hitWater.collider.gameObject.GetComponent<Remana>().CallPickup();
            }
        }

    }

    void Shooting()
    {
        if (CancleFire != true && fireOnCooldown == false)
        {
            StartCoroutine(Shoot("Cooldown"));
            fireOnCooldown = true;
        }
        else
        {
            allowFire = false;
        }
        return;
        IEnumerator Shoot(string context)
        {
            if (context == "Cooldown")
            {
                allowFire = false;
                _gunScript.Shoot();
                yield return new WaitForSeconds(_playerManager.fireRate);
                fireOnCooldown = false;
                allowFire = true;
            }
        }
    }
    public void Move(InputAction.CallbackContext context)
    {
        if (_playerManager._Status != MultiStatus.Stunt)
        {
            _input = context.ReadValue<Vector2>();
            _direction = new Vector3(_input.x, 0.0f, _input.y);
            if (camera1 != null) // เพิ่มเงื่อนไขตรวจสอบว่า camera1 ไม่เป็น null
            {
                _direction = Quaternion.Euler(0, camera1.gameObject.transform.eulerAngles.y, 0) * _direction;
            }
            _playerManager._Status = MultiStatus.Move;
            if (_direction == Vector3.zero)
            {
                _pAnimation.SetBool("Moving", false);
                _playerManager._Status = MultiStatus.Idle;
            }
            else
            {
                _pAnimation.SetBool("Moving", true);
            }
        }
    }
    public void MousePosition(InputAction.CallbackContext context)
    {
        _mousepos = context.action.ReadValue<Vector2>();
    }
    public void Inter(InputAction.CallbackContext context)
    {
        
    }
    public void Fire(InputAction.CallbackContext context)
    {
        if (!Pv.IsMine) return; // ตรวจสอบว่าเป็น player ตัวเองหรือไม่
        allowFire = true;
        CancleFire = false;
        if (context.canceled)
        {
            allowFire = false;
            CancleFire = true;
        }
    }
    public void SecondaryFire(InputAction.CallbackContext context)
    {
        if (!Pv.IsMine) return;
        
        if (context.started && onDash == false && _direction != Vector3.zero)
        {
            StartCoroutine(Dash());
            StartCoroutine(DashOnCooldown());
        }
    }

    public void Skill(InputAction.CallbackContext context)
    {
        if (context.performed && _playerManager.isCooldown != true && _playerManager._Element != MultiCurrentElement.None)
        {
            _playerManager.ActivateAbility();
            switch (_playerManager._Element)
            {
                case MultiCurrentElement.Water:
                    PhotonNetwork.Instantiate(_playerManager.Skill[0].name, new Vector3(transform.position.x,1.8f,transform.position.z), transform.rotation);
                    break;
                case MultiCurrentElement.Earth:
                    PhotonNetwork.Instantiate(_playerManager.Skill[1].name, new Vector3(transform.position.x,1.8f,transform.position.z), transform.rotation);
                    break;
                case MultiCurrentElement.Wind:
                    StartCoroutine(Dash());
                    PhotonNetwork.Instantiate(_playerManager.Skill[2].name, new Vector3(transform.position.x,1.8f,transform.position.z), transform.rotation);
                    _playerManager.invincibillityCounter = 0.25f;
                    break;
            }
        }
    }
    public void Ult(InputAction.CallbackContext context)
    {
        if (context.performed && _playerManager.curUltEnergy == _playerManager.MaxultEnergy && _playerManager._Element != MultiCurrentElement.None)
        {
            _playerManager.curUltEnergy = 0;
            Debug.Log("Ultpress");
            switch (_playerManager._Element)
            {
                case MultiCurrentElement.Water:
                    PhotonNetwork.Instantiate(_playerManager.ult[0].name, new Vector3(transform.position.x,1.8f,transform.position.z), transform.rotation);
                    break;
                case MultiCurrentElement.Earth:
                    PhotonNetwork.Instantiate(_playerManager.ult[1].name, new Vector3(transform.position.x,1.8f,transform.position.z), transform.rotation);
                    break;
                case MultiCurrentElement.Wind:
                    PhotonNetwork.Instantiate(_playerManager.ult[2].name, new Vector3(transform.position.x,1.8f,transform.position.z), transform.rotation);
                    break;
            }
        }
    }
    IEnumerator Dash()
    {
        float startTime = Time.time;
        while (Time.time < startTime + 0.15f)
        {
            _characterController.Move(_direction * _playerManager.DashRange * Time.deltaTime);
            _trailRenderer.emitting = true;
            yield return null;
        }
        _trailRenderer.emitting = false;
    }
    IEnumerator DashOnCooldown()
    {
        onDash = true;
        _playerManager.invincibillityCounter = 0.25f;
        yield return new WaitForSeconds(_playerManager.DashCooldown);
        onDash = false;
    }
}
