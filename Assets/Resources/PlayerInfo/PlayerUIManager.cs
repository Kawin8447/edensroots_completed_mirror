using System;
using Cinemachine;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIManager : MonoBehaviourPunCallbacks, IPunObservable
{
    public TextMeshProUGUI _HealthText = null;

    public int healthP1 = 100;
    
    //PhotonView photonView;

    PlayerController _playerController;

    private float _currentHealth;

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //sync health
        if (stream.IsWriting)
        {
            stream.SendNext(healthP1);
        }
        else
        {
            //reading
            healthP1 = (int)stream.ReceiveNext();
            
        }
    }

    private void FixedUpdate()
    {
        _HealthText.text = healthP1.ToString();
    }

    void TakeDamage(int damage)
    {
        healthP1 -= damage;
    }
}
