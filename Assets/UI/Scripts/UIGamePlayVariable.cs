using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class UIGamePlayVariable : MonoBehaviour
{
    
    protected const string GamePlayScreen = "V--GamePlay";
    protected const string CurrentHp = "Current--Hp";
    protected const string OverHeal = "Current--Hp--Overheal";
    protected const string CurrentHpText = "Current--Hp--text";
    protected const string QSkillCharge = "Skill--charge--Q";
    protected const string QCoolDownScrim = "CD--scrim--Q";
    protected const string QCoolDownTimeText = "CDTime--text--Q";
    protected const string ESkillCharge = "Skill--charge--E";
    protected const string ECoolDownScrim = "CD--scrim--E";
    protected const string ECoolDownTimeText = "CDTime--text--E";
    protected const string DashVisual = "Dash";
    protected const string CurrentMana = "Current--Mana";
    protected const string ManaOverflow = "Current--Mana--Overflow";
    protected const string CurrentManaText = "Current--Mana--text";
    protected const string CurrentDisplayElement = "Current--Display--Element";
    protected const string SkillQ = "Skill--Q";
    protected const string SkillE = "Skill";

    protected VisualElement _gamePlayScreen;
    protected VisualElement _currentHp;
    protected VisualElement _overHeal;
    protected Label _currentHpText;
    protected VisualElement _qSkillCharge;
    protected VisualElement _qCoolDownScrim;
    protected Label _qCoolDownTimeText;
    protected VisualElement _eSkillCharge;
    protected VisualElement _eCoolDownScrim;
    protected Label _eCoolDownTimeText;
    protected VisualElement _dashVisual;
    protected VisualElement _currentMana;
    protected VisualElement _manaOverflow;
    protected Label _currentManaText;
    protected VisualElement _currentDisplayElement;
    protected VisualElement _skillQ;
    protected VisualElement _skillE;
    void Start()
    {
        var root = GetComponent<UIDocument>().rootVisualElement;

        _gamePlayScreen = root.Q<VisualElement>(GamePlayScreen);
        
        _currentHp = root.Q<VisualElement>(CurrentHp);
        _overHeal = root.Q<VisualElement>(OverHeal);
        _currentHpText = root.Q<Label>(CurrentHpText);
        
        
        _qSkillCharge = root.Q<VisualElement>(QSkillCharge);
        _qCoolDownScrim = root.Q<VisualElement>(QCoolDownScrim);
        _qCoolDownTimeText = root.Q<Label>(QCoolDownTimeText);
        
        _eSkillCharge = root.Q<VisualElement>(ESkillCharge);
        _eCoolDownScrim = root.Q<VisualElement>(ECoolDownScrim);
        _eCoolDownTimeText = root.Q<Label>(ECoolDownTimeText);
        
        _dashVisual = root.Q<VisualElement>(DashVisual);
        
        _currentMana = root.Q<VisualElement>(CurrentMana);
        _manaOverflow = root.Q<VisualElement>(ManaOverflow);
        _currentManaText = root.Q<Label>(CurrentManaText);
        
        _currentDisplayElement = root.Q<VisualElement>(CurrentDisplayElement);

        _skillQ = root.Q<VisualElement>(SkillQ);
        _skillE = root.Q<VisualElement>(SkillE);
    }
    
}
