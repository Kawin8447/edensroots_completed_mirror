using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class UIOperator : UIVariableStore
{
    public static UIOperator Instance;

    [SerializeField] public UIPages currentState = UIPages.GameTitle;

    [SerializeField] public GameObject backGround;
    bool anyKeyWasPressed = false;

    private void Awake()
    {
        Instance = this;
    }

    protected override void Start()
    {
        base.Start();

        AssignButtonClick();
        AssignButtonPress();

    }

    void AssignButtonClick()
    {
        _buttonStart.RegisterCallback<ClickEvent>(OnStartClicked);
        _buttonSinglePlayer.RegisterCallback<ClickEvent>(OnSinglePlayerClicked);
        _buttonMultiPlayer.RegisterCallback<ClickEvent>(OnMultiPlayerClicked);
        _buttonExit.RegisterCallback<ClickEvent>(OnExitClicked);
        _buttonReturnSingle.RegisterCallback<ClickEvent>(OnReturnSingleClicked);
        _buttonCampaignI.RegisterCallback<ClickEvent>(OnCampaignIClicked);
        _buttonCampaignIi.RegisterCallback<ClickEvent>(OnCampaignIiClicked);
        _buttonCampaignIii.RegisterCallback<ClickEvent>(OnCampaignIiiClicked);
        _buttonReturnMulti.RegisterCallback<ClickEvent>(OnReturnMultiClicked);
        _buttonHostRoom.RegisterCallback<ClickEvent>(OnHostRoomClicked);
        _buttonFindRoom.RegisterCallback<ClickEvent>(OnFindRoomClicked);
        _buttonYes.RegisterCallback<ClickEvent>(OnYesClicked);
        _buttonNo.RegisterCallback<ClickEvent>(OnNoClicked);
    }
    void AssignButtonPress()
    {
        _buttonStart.RegisterCallback<KeyDownEvent>(OnStartPressed);
        _buttonSinglePlayer.RegisterCallback<KeyDownEvent>(OnSinglePlayerPressed);
        _buttonMultiPlayer.RegisterCallback<KeyDownEvent>(OnMultiPlayerPressed);
        _buttonExit.RegisterCallback<KeyDownEvent>(OnExitPressed);
        _buttonReturnSingle.RegisterCallback<KeyDownEvent>(OnReturnSinglePressed);
        _buttonCampaignI.RegisterCallback<KeyDownEvent>(OnCampaignIPressed);
        _buttonCampaignIi.RegisterCallback<KeyDownEvent>(OnCampaignIiPressed);
        _buttonCampaignIii.RegisterCallback<KeyDownEvent>(OnCampaignIiiPressed);
        _buttonReturnMulti.RegisterCallback<KeyDownEvent>(OnReturnMultiPressed);
        _buttonHostRoom.RegisterCallback<KeyDownEvent>(OnHostRoomPressed);
        _buttonFindRoom.RegisterCallback<KeyDownEvent>(OnFindRoomPressed);
        _buttonYes.RegisterCallback<KeyDownEvent>(OnYesPressed);
        _buttonNo.RegisterCallback<KeyDownEvent>(OnNoPressed);
    }


    #region Click Event

    void OnStartClicked(ClickEvent evtData)
    {
        ChangeFakeScene(UIPages.SelectMenu, true);
    }

    void OnSinglePlayerClicked(ClickEvent evtData)
    {
        ChangeFakeScene(UIPages.SinglePlayer, true);
    }

    void OnMultiPlayerClicked(ClickEvent evtData)
    {
        ChangeFakeScene(UIPages.MultiPlayer, true);

        Launcher.Instance.StartConectionProcess();
    }

    void OnExitClicked(ClickEvent evtData)
    {
        CallConfirmScreen();
    }

    void OnReturnSingleClicked(ClickEvent evtData)
    {
        ChangeFakeScene(UIPages.SelectMenu, true);
    }

    void OnCampaignIClicked(ClickEvent evtData)
    {
        ChangeFakeScene(UIPages.None, true);
        SceneManager.LoadScene(1);
    }

    void OnCampaignIiClicked(ClickEvent evtData)
    {
        ChangeFakeScene(UIPages.None, true);
        SceneManager.LoadScene(3);
    }

    void OnCampaignIiiClicked(ClickEvent evtData)
    {
        ChangeFakeScene(UIPages.None, true);
        SceneManager.LoadScene(4);
    }

    void OnReturnMultiClicked(ClickEvent evtData)
    {
        ChangeFakeScene(UIPages.SelectMenu, true);
        Launcher.Instance.DisconnectFromServer();
        MenuManager.Instance.OpenMenu("none");
        //HideMultiplayerMenu();
    }

    void OnHostRoomClicked(ClickEvent evtData)
    {
        MenuManager.Instance.OpenMenu("create room");
        HideMultiplayerMenu();
    }

    void OnFindRoomClicked(ClickEvent evtData)
    {
        MenuManager.Instance.OpenMenu("find room");
        HideMultiplayerMenu();
    }

    void OnNoClicked(ClickEvent evtData)
    {
        _confirmScreen.style.display = DisplayStyle.None;
    }

    void OnYesClicked(ClickEvent evtData)
    {
        if (currentState == UIPages.SelectMenu)
        {
            Application.Quit();
        }
    }

    #endregion

    public void HideMultiplayerMenu()
    {
        ChangeFakeScene(UIPages.None, true);
    }

    public void ShowMultiplayerMenu()
    {
        ChangeFakeScene(UIPages.MultiPlayer, true);
    }

    #region Press Event

    void OnStartPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            ChangeFakeScene(UIPages.SelectMenu, true);
        }
    }

    void OnSinglePlayerPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            ChangeFakeScene(UIPages.SinglePlayer, true);
        }
    }

    void OnMultiPlayerPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            ChangeFakeScene(UIPages.MultiPlayer, true);
        }

    }

    void OnExitPressed(KeyDownEvent evtData)
    {
        //ออกเกมอะ bro
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            CallConfirmScreen();
        }
    }

    void OnReturnSinglePressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            ChangeFakeScene(UIPages.SelectMenu, true);
        }

    }

    void OnCampaignIPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            ChangeFakeScene(UIPages.None, true);
            SceneManager.LoadScene(1);
        }

    }

    void OnCampaignIiPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            ChangeFakeScene(UIPages.None, true);
            SceneManager.LoadScene(3);
        }
    }

    void OnCampaignIiiPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            ChangeFakeScene(UIPages.None, true);
            SceneManager.LoadScene(4);
        }
    }

    void OnReturnMultiPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            ChangeFakeScene(UIPages.SelectMenu, true);
        }

    }

    void OnHostRoomPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            MenuManager.Instance.OpenMenu("create room");
            HideMultiplayerMenu();
        }
    }

    void OnFindRoomPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            MenuManager.Instance.OpenMenu("find room");
            HideMultiplayerMenu();
        }
    }
    void OnNoPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            _confirmScreen.style.display = DisplayStyle.None;
        }
    }

    void OnYesPressed(KeyDownEvent evtData)
    {
        if (evtData.keyCode == KeyCode.Space || evtData.keyCode == KeyCode.Return)
        {
            if (currentState == UIPages.SelectMenu)
            {
                Application.Quit();
            }
        }
    }


    #endregion

    #region Operating


    void CallConfirmScreen()
    {
        _confirmScreen.style.display = DisplayStyle.Flex;
        switch (currentState)
        {
            case UIPages.SelectMenu:
                _textWarning.text = "Exit Game";
                break;
        }
    }

    void ChangeFakeScene(UIPages cp, bool visiblebackground)
    {
        currentState = cp;
        UpdatePageVisibility();
        backGround.SetActive(visiblebackground);
    }

    public void UpdatePageVisibility()
    {
        Dictionary<UIPages, VisualElement> pageElements = new Dictionary<UIPages, VisualElement>()
        {
            { UIPages.GameTitle, _gameTitle },
            { UIPages.SelectMenu, _selectMenu },
            { UIPages.SinglePlayer, _singlePlayer },
            { UIPages.MultiPlayer, _multiPlayer }
        };
        foreach (var kvp in pageElements)
        {
            kvp.Value.style.display = currentState == kvp.Key ? DisplayStyle.Flex : DisplayStyle.None;

        }
    }

    #endregion

    private void Update()
    {
        UpdatePageVisibility();
        switch (currentState)
        {
            case UIPages.SinglePlayer:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    _buttonReturnSingle.Focus();
                    anyKeyWasPressed = false;
                }
                if (Input.GetKeyDown(KeyCode.D))
                {
                    if (anyKeyWasPressed == false)
                    {
                        _buttonCampaignI.Focus();
                        anyKeyWasPressed = true;
                    }
                }
                if (Input.GetKeyDown(KeyCode.S))
                {
                    _buttonCampaignI.Focus();
                }
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ChangeFakeScene(UIPages.SelectMenu, true);
                }
                break;

            case UIPages.MultiPlayer:
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    _buttonReturnMulti.Focus();
                    anyKeyWasPressed = false;
                }
                if (Input.GetKeyDown(KeyCode.D))
                {
                    if (anyKeyWasPressed == false)
                    {
                        _buttonHostRoom.Focus();
                        anyKeyWasPressed = true;
                    }
                }
                if (Input.GetKeyDown(KeyCode.S))
                {
                    _buttonHostRoom.Focus();
                }
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    ChangeFakeScene(UIPages.SelectMenu, true);
                }
                break;

            case UIPages.SelectMenu:
                if (_confirmScreen.style.display == DisplayStyle.Flex)
                {
                    if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.W))
                    {
                        _buttonYes.Focus();
                    }
                    else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.S))
                    {
                        _buttonNo.Focus();
                    }
                }
                else

                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    CallConfirmScreen();
                }
                break;
        }

    }
}

