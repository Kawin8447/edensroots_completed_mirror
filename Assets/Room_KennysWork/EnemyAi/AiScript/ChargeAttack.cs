using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ChargeAttack : MonoBehaviour
{
    public enum ElementType
    {
        None,
        Water,
        Earth,
        Wind
    }
    public ElementType AttackElement;
    public float damageAmount = 20;

    private void OnTriggerEnter(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable != null && !other.gameObject.CompareTag("Enemy"))
        {
            switch (AttackElement)
            {
                case ElementType.None:
                    if (GameRoomManager.Instance._currentElementVar == "None"
                    || GameRoomManager.Instance._currentElementVar == "Water"
                    || GameRoomManager.Instance._currentElementVar == "Earth"
                    || GameRoomManager.Instance._currentElementVar == "Wind")
                    {
                        damageable.DoDamage(damageAmount);
                    }
                    break;
                case ElementType.Water:
                    if (GameRoomManager.Instance._currentElementVar == "Earth")
                    {
                        damageable.DoDamage(damageAmount + (damageAmount * 0.5f));
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Wind")
                    {
                        damageable.DoDamage(damageAmount / 2);
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Water"
                    || GameRoomManager.Instance._currentElementVar == "None")
                    {
                        damageable.DoDamage(damageAmount);
                    }
                    break;
                case ElementType.Earth:
                    if (GameRoomManager.Instance._currentElementVar == "Wind")
                    {
                        damageable.DoDamage(damageAmount + (damageAmount * 0.5f));
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Water")
                    {
                        damageable.DoDamage(damageAmount / 2);
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Earth"
                    || GameRoomManager.Instance._currentElementVar == "None")
                    {
                        damageable.DoDamage(damageAmount);
                    }
                    break;
                case ElementType.Wind:
                    if (GameRoomManager.Instance._currentElementVar == "Water")
                    {
                        damageable.DoDamage(damageAmount + (damageAmount * 0.5f));
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Earth")
                    {
                        damageable.DoDamage(damageAmount / 2);
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Wind"
                    || GameRoomManager.Instance._currentElementVar == "None")
                    {
                        damageable.DoDamage(damageAmount);
                    }
                    break;
            }
        }
    }
}
