using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.TextCore.Text;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using Photon.Realtime;
using Unity.Mathematics;
using Unity.VisualScripting;

public class EnemyBossAi : MonoBehaviour, Damageable
{
    public float Health;
    private float Maxhealth;
    private float currentHealth;

    public Image HealthBar;
    public TextMeshProUGUI HealthText;

    [Header("Damage Indecator")]
    public Animator DamageAnim;
    public TextMeshProUGUI DamageText;
    private float TotalDamage;

    public static event System.Action<bool> BossRoomClear;
    private float FinalDamage;
    private string PlayerElement;

    public GameObject[] EnemyPrefab;
    public float SkillRadius = 5f;
    private ParticleSystem ChargeEffect;

    private bool IsImmune = false;
    private bool CanSummon = true;
    private bool Casting = false;
    private bool CanAttack = true;

    private GameObject _player;

    public bool PlayerIsInRange;
    public LayerMask WhatIsPlayer;
    public GameObject FakeProjectile;
    private string BreakElement;
    public float BreakArmor;
    private float Armor;
    private bool ChannelingSuccess;

    public GameObject ArmorBar;
    public Image ArmorBarFill;
    public TextMeshProUGUI ArmorText;

    public GameObject ArmorHandle;
    public GameObject[] ArmorPlate;
    public Material[] ArmorMat;
    [SerializeField]private Animator bossAnim;

    private void Awake()
    {
        Maxhealth = Health;
        currentHealth = Health;

        DamageText.gameObject.SetActive(false);
        ChargeEffect = gameObject.GetComponent<ParticleSystem>();

        ArmorBar.gameObject.SetActive(false);
        ArmorHandle.gameObject.SetActive(false);
    }
    private void Start()
    {
        _player = GameObject.Find("PlayerManage(Clone)");
        ChannelingSuccess = true;
    }
    private void Update()
    {
        PlayerIsInRange = Physics.CheckSphere(transform.position, SkillRadius, WhatIsPlayer);

        //for Hp bar
        /* if (currentHealth != Health)
        {
            if (currentHealth > Health)
            {
                currentHealth--;
            }

            if (currentHealth < Health)
            {
                currentHealth++;
            }
        } */

        currentHealth = Health;

        HealthBar.fillAmount = currentHealth / Maxhealth;
        HealthText.text = Mathf.Round(currentHealth).ToString();

        if (PlayerIsInRange)
        {
            Debug.LogWarning("Player is in range");
            if (CanSummon)
            {
                CanSummon = false;
                StartCoroutine(SummoningSkill());
            }
            if (!Casting && CanAttack)
            {
                bossAnim.SetTrigger("Attack");
                Instantiate(FakeProjectile, transform.position, quaternion.identity);
                CanAttack = false;
                StartCoroutine(ResetAttack());
            }
            if (!Casting)
            {
                bossAnim.SetBool("Charge Attack",false);
            }
        }

        ArmorBarFill.fillAmount = Armor / BreakArmor;
        ArmorText.text = Mathf.Round(Armor).ToString();

        for (int i = 0; i < ArmorPlate.Length; i++)
        {
            if (BreakElement == "Wind")
            {
                ArmorPlate[i].gameObject.GetComponent<MeshRenderer>().material = ArmorMat[0];
            }
            if (BreakElement == "Water")
            {
                ArmorPlate[i].gameObject.GetComponent<MeshRenderer>().material = ArmorMat[1];
            }
            if (BreakElement == "Earth")
            {
                ArmorPlate[i].gameObject.GetComponent<MeshRenderer>().material = ArmorMat[2];
            }
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, SkillRadius);
    }

    public void RandomElement()
    {
        int random = Random.Range(1, 4);
        if (random == 1) { BreakElement = "Water"; }
        if (random == 2) { BreakElement = "Earth"; }
        if (random == 3) { BreakElement = "Wind"; }
    }
    public void DoDamage(float damage)
    {
        FinalDamage = 0f;
        PlayerElement = GameRoomManager.Instance._currentElementVar;

        if (PlayerElement != "None" && !IsImmune)
        {
            FinalDamage = damage;
            DamageText.color = Color.red;
            DamageText.gameObject.SetActive(true);
            SetupDamage(FinalDamage);
        }
        else if (PlayerElement == "None" && !IsImmune)
        {
            DamageText.gameObject.SetActive(true);
            DamageText.text = "Immune";
            DamageText.color = Color.gray;
            DamageAnim.SetTrigger("GetAnDamage");
        }
        else if (Casting && BreakElement != PlayerElement)
        {
            DamageText.gameObject.SetActive(true);
            DamageText.text = "Immune Channeling";
            DamageText.color = Color.gray;
            DamageAnim.SetTrigger("GetAnDamage");
        }

        else if (Casting && BreakElement == PlayerElement)
        {
            DamageText.gameObject.SetActive(true);
            Armor -= damage;
            DamageText.text = "Break " + damage;
            DamageText.color = Color.yellow;
            DamageAnim.SetTrigger("GetAnDamage");


            if (Armor <= 0)
            {
                Armor = 0;
            }
        }
    }
    private void SetupDamage(float ShowUpDamage)
    {
        Health -= ShowUpDamage;

        Debug.Log("Ai take damage by bullet HP = " + Health);
        if (Health <= 0)
        {
            Invoke(nameof(DestryEnemy), 0.5f);
        }

        if (DamageAnim.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.33f)
        {
            TotalDamage += ShowUpDamage;
            DamageText.text = TotalDamage.ToString();
            DamageAnim.SetTrigger("GetAnDamage");
        }
        else
        {
            TotalDamage = ShowUpDamage;
            DamageText.text = TotalDamage.ToString();
            DamageAnim.SetTrigger("GetAnDamage");
        }
    }
    private void DestryEnemy()
    {
        Debug.Log("Boss has been Clear");
        BossRoomClear?.Invoke(true);
        Destroy(gameObject);
    }
    public int SummonAmount = 3;

    IEnumerator SummoningSkill()
    {
        Casting = true;
        //delay

        yield return new WaitForSeconds(2f);

        ChargeEffect.Play();
        bossAnim.SetBool("Charge Start",true);
        IsImmune = true;

        RandomElement();

        ArmorBar.gameObject.SetActive(true);
        ArmorHandle.gameObject.SetActive(true);

        ChannelingSuccess = true;
        Armor = BreakArmor;

        yield return new WaitForSeconds(5f);

        if (Armor > 0)
        {
            ChannelingSuccess = true;
        }
        else if (Armor <= 0)
        {
            ChannelingSuccess = false;
        }

        yield return new WaitForSeconds(0.25f);

        ArmorBar.gameObject.SetActive(false);
        ArmorHandle.gameObject.SetActive(false);

        if (ChannelingSuccess)
        {
            Summon();
            ChargeEffect.Stop();
            bossAnim.SetBool("Charge Start",false);
            bossAnim.SetBool("Charge Attack",true);
            IsImmune = false;
            Casting = false;
        }
        else if (!ChannelingSuccess)
        {
            SemiSummon();
            ChargeEffect.Stop();
            bossAnim.SetBool("Charge Start",false);
            bossAnim.SetBool("Charge Attack",true);
            IsImmune = false;
            Casting = false;

            DamageText.color = Color.magenta;
            DamageText.gameObject.SetActive(true);
            SetupDamage(500);
        }
       
        
        yield return new WaitForSeconds(18f);
        CanSummon = true;
        SummonAmount += 1;
    }
    private void Summon()
    {
        for (int i = 0; i < SummonAmount; i++)
        {
            float spawnRadius = SkillRadius;
            float randomAngle = Random.Range(0f, 2f * Mathf.PI);

            float newSpawnPosX = transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
            float newSpawnPosZ = transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

            Vector3 SpawnPos = new Vector3(newSpawnPosX, transform.position.y, newSpawnPosZ);

            GameObject AmountPrefab = EnemyPrefab[Random.Range(0, EnemyPrefab.Length)];
            Instantiate(AmountPrefab, SpawnPos, quaternion.identity);
        }
    }
    private void SemiSummon()
    {
        for (int i = 0; i < 1; i++)
        {
            float spawnRadius = SkillRadius;
            float randomAngle = Random.Range(0f, 2f * Mathf.PI);

            float newSpawnPosX = transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
            float newSpawnPosZ = transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

            Vector3 SpawnPos = new Vector3(newSpawnPosX, transform.position.y, newSpawnPosZ);

            GameObject AmountPrefab = EnemyPrefab[Random.Range(0, EnemyPrefab.Length)];
            Instantiate(AmountPrefab, SpawnPos, quaternion.identity);
        }
    }
    IEnumerator ResetAttack()
    {

        yield return new WaitForSeconds(6f);

        CanAttack = true;
    }
}
