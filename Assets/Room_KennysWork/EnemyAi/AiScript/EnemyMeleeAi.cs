using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.TextCore.Text;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using Unity.VisualScripting;
using Unity.Mathematics;

public class EnemyMeleeAi : MonoBehaviour, Damageable
{
    public string PlayerObject;

    public NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    public float Health;
    private float Maxhealth;
    private float currentHealth;

    public Image HealthBar;
    public TextMeshProUGUI HealthText;

    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    //Attacking
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public GameObject[] Projectiles;

    //States
    public float sightRange, attackRange, SkillRange;
    public bool playerInSightRange, playerInAttackRange, playerInSkillRange;

    public GameObject shooterPoint;

    public static event System.Action<bool> MeleeRoomClear;

    [Header("Damage Indecator")]
    public Animator DamageAnim;
    public TextMeshProUGUI DamageText;
    private float TotalDamage;

    public CapsuleCollider MeleeCollider;

    [Header("Dash")]
    public bool CanDash, IsDashing;
    public float DashCooldown = 5;

    float distanceToPlayer;

    private string AiElement;
    private string PlayerElement;

    public Material[] Colors;

    private float FinalDamage;
    private ParticleSystem ChargeEffect;

    public GameObject[] ChargeAttackHitBox;
    [Header("EnemySkin")] 
    [SerializeField]private GameObject _body;
    [SerializeField]private GameObject _lFeet;
    [SerializeField]private GameObject _rFeet;
    [SerializeField]private GameObject _lHand;
    [SerializeField]private GameObject _rHand;
    [SerializeField]private Animator enemyAnim;
    private bool isCharging;

    private void Awake()
    {
        player = GameObject.Find(PlayerObject).transform;
        agent = GetComponent<NavMeshAgent>();

        Maxhealth = Health;
        currentHealth = Health;

        DamageText.gameObject.SetActive(false);

        int random = Random.Range(1, 5);
        if (random == 1) { AiElement = "None"; }
        if (random == 2) { AiElement = "Water"; }
        if (random == 3) { AiElement = "Earth"; }
        if (random == 4) { AiElement = "Wind"; }
        ChargeEffect = gameObject.GetComponent<ParticleSystem>();
    }

    private void Start()
    {
        CanDash = true;
        IsDashing = false;

        if (AiElement == "Normal")
        {
            _body.GetComponent<SkinnedMeshRenderer>().material = Colors[0];
            _lFeet.GetComponent<SkinnedMeshRenderer>().material = Colors[0];
            _rFeet.GetComponent<SkinnedMeshRenderer>().material = Colors[0];
            _lHand.GetComponent<SkinnedMeshRenderer>().material = Colors[0];
            _rHand.GetComponent<SkinnedMeshRenderer>().material = Colors[0];
        }
        if (AiElement == "Water")
        {
            _body.GetComponent<SkinnedMeshRenderer>().material = Colors[1];
            _lFeet.GetComponent<SkinnedMeshRenderer>().material = Colors[1];
            _rFeet.GetComponent<SkinnedMeshRenderer>().material = Colors[1];
            _lHand.GetComponent<SkinnedMeshRenderer>().material = Colors[1];
            _rHand.GetComponent<SkinnedMeshRenderer>().material = Colors[1];
        }
        if (AiElement == "Earth")
        {
            _body.GetComponent<SkinnedMeshRenderer>().material = Colors[2];
            _lFeet.GetComponent<SkinnedMeshRenderer>().material = Colors[2];
            _rFeet.GetComponent<SkinnedMeshRenderer>().material = Colors[2];
            _lHand.GetComponent<SkinnedMeshRenderer>().material = Colors[2];
            _rHand.GetComponent<SkinnedMeshRenderer>().material = Colors[2];
        }
        if (AiElement == "Wind")
        {
            _body.GetComponent<SkinnedMeshRenderer>().material = Colors[3];
            _lFeet.GetComponent<SkinnedMeshRenderer>().material = Colors[3];
            _rFeet.GetComponent<SkinnedMeshRenderer>().material = Colors[3];
            _lHand.GetComponent<SkinnedMeshRenderer>().material = Colors[3];
            _rHand.GetComponent<SkinnedMeshRenderer>().material = Colors[3];
        }

        ChargeAttackHitBox[0].SetActive(false);
        ChargeAttackHitBox[1].SetActive(false);
        ChargeAttackHitBox[2].SetActive(false);
        ChargeAttackHitBox[3].SetActive(false);
    }

    private void Update()
    {
        //Check for sight and attack range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);
        playerInSkillRange = Physics.CheckSphere(transform.position, SkillRange, whatIsPlayer);


        if (Health > 0 && !playerInSightRange && !playerInAttackRange && !IsDashing) Patroling();
        if (Health > 0 && playerInSightRange && !playerInAttackRange && !IsDashing) ChasePlayer();
        if (Health > 0 && playerInAttackRange && playerInSightRange && !IsDashing) AttackPlayer();

        if (Health > 0 && playerInSightRange && playerInSkillRange && !playerInAttackRange && !IsDashing)
        {
            {
                System.Action[] skills = new System.Action[] { Skill_Dash };

                int randomIndex = Random.Range(0, skills.Length);

                skills[randomIndex]();
            }
        }

        //for Hp bar
        if (currentHealth != Health)
        {
            if (currentHealth > Health)
            {
                currentHealth--;
            }

            if (currentHealth < Health)
            {
                currentHealth++;
            }
        }

        HealthBar.fillAmount = currentHealth / Maxhealth;
        HealthText.text = Mathf.Round(currentHealth).ToString();

        if (IsDashing) Dashing();
    }

    private void Patroling()
    {
        enemyAnim.SetBool("IsMove",true);
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        if (isCharging)
        {
            enemyAnim.SetBool("IsMove",false);
        }
        else
        {
            enemyAnim.SetBool("IsMove",true);
            enemyAnim.SetBool("Charge Attack",false);
        }
        transform.LookAt(player);
        agent.SetDestination(player.position);
    }

    private void AttackPlayer()
    {
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);
        enemyAnim.SetTrigger("Attack");
        transform.LookAt(player);

        if (!alreadyAttacked)
        {
            ////Attack code
            if (AiElement == "None")
            {
                Rigidbody rb = Instantiate(Projectiles[0], shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
                rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            }
            if (AiElement == "Water")
            {
                Rigidbody rb = Instantiate(Projectiles[1], shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
                rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            }
            if (AiElement == "Earth")
            {
                Rigidbody rb = Instantiate(Projectiles[2], shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
                rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            }
            if (AiElement == "Wind")
            {
                Rigidbody rb = Instantiate(Projectiles[3], shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
                rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            }

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, SkillRange);
    }

    private void DestryEnemy()
    {
        if (GameRoomManager.Instance.CurrentEnemyCount > 0)
        {
            GameRoomManager.Instance.CurrentEnemyCount -= 1;

            Debug.Log("Current Enemy = " + GameRoomManager.Instance.CurrentEnemyCount);

            if (GameRoomManager.Instance.CurrentEnemyCount == 0)
            {
                Debug.Log("Room Clear");
                MeleeRoomClear?.Invoke(true);
            }
        }
        SpawnElement();
        Destroy(gameObject);
    }

    public void DoDamage(float damage)
    {
        FinalDamage = 0f;
        PlayerElement = GameRoomManager.Instance._currentElementVar;

        if (AiElement == "None")
        {
            if (PlayerElement == "None") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Water") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Earth") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Wind") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
        }
        else if (AiElement == "Water")
        {
            if (PlayerElement == "None") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Water") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Earth") { FinalDamage = (damage / 2); DamageText.color = Color.gray; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Wind") { FinalDamage = (damage * 2); DamageText.color = Color.red; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
        }
        else if (AiElement == "Earth")
        {
            if (PlayerElement == "None") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Water") { FinalDamage = (damage * 2); DamageText.color = Color.red; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Earth") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Wind") { FinalDamage = (damage / 2); DamageText.color = Color.gray; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
        }
        else if (AiElement == "Wind")
        {
            if (PlayerElement == "None") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Water") { FinalDamage = (damage / 2); DamageText.color = Color.gray; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Earth") { FinalDamage = (damage * 2); DamageText.color = Color.red; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Wind") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
        }
    }
    private void SetupDamage(float ShowUpDamage)
    {
        Health -= ShowUpDamage;

        Debug.Log("Ai take damage by bullet HP = " + Health);
        if (Health <= 0)
        {
            agent.enabled = false;
            Invoke(nameof(DestryEnemy), 0.5f);
        }

        if (DamageAnim.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.33f)
        {
            TotalDamage += ShowUpDamage;
            DamageText.text = TotalDamage.ToString();
            DamageAnim.SetTrigger("GetAnDamage");
        }
        else
        {
            TotalDamage = ShowUpDamage;
            DamageText.text = TotalDamage.ToString();
            DamageAnim.SetTrigger("GetAnDamage");
        }
    }

    private void Skill_Dash()
    {
        if (CanDash == true)
        {
            StartCoroutine(Dash());
        }
    }
    IEnumerator Dash()
    {
        //start stay
        ChargeEffect.Play();
        isCharging = true;
        enemyAnim.SetBool("isMove",false);
        enemyAnim.SetBool("Charge Start",true);
        CreateChargeAttackHitBox();
        CanDash = false;
        agent.isStopped = true;

        //stay for 1s
        yield return new WaitForSeconds(1f);

        //start dash
        ChargeEffect.Stop();
        isCharging = false;
        enemyAnim.SetBool("Charge Start",false);
        enemyAnim.SetBool("Charge Attack",true);
        IsDashing = true;
        transform.LookAt(transform.position + transform.forward);
        distanceToPlayer = Vector3.Distance(transform.position, player.position);
        Debug.LogWarning("distanceToPlayer = " + distanceToPlayer);

        if (distanceToPlayer > 12) distanceToPlayer = 12;

        //dash for Range sec
        yield return new WaitForSeconds(distanceToPlayer / 10);

        //start reset
        HideChargeAttackHitBox();
        IsDashing = false;
        agent.isStopped = false;
        agent.acceleration = 0;

        yield return new WaitForSeconds(.1f);
        agent.SetDestination(transform.position);
        agent.acceleration = 120;

        //wait for cooldown
        yield return new WaitForSeconds(DashCooldown);

        //done reset
        CanDash = true;
    }

    private void Dashing()
    {
        agent.Move(transform.forward * (distanceToPlayer * 2) * Time.deltaTime);
    }
    private void CreateChargeAttackHitBox()
    {
        if (AiElement == "None") { ChargeAttackHitBox[0].SetActive(true); }
        if (AiElement == "Water") { ChargeAttackHitBox[1].SetActive(true); }
        if (AiElement == "Earth") { ChargeAttackHitBox[2].SetActive(true); }
        if (AiElement == "Wind") { ChargeAttackHitBox[3].SetActive(true); }
    }
    private void HideChargeAttackHitBox()
    {
        if (AiElement == "None") { ChargeAttackHitBox[0].SetActive(false); }
        if (AiElement == "Water") { ChargeAttackHitBox[1].SetActive(false); }
        if (AiElement == "Earth") { ChargeAttackHitBox[2].SetActive(false); }
        if (AiElement == "Wind") { ChargeAttackHitBox[3].SetActive(false); }
    }
    [Header("Element Orbs")]
    public GameObject[] ElementOrbs;
    private void SpawnElement()
    {
        int SpawnChange = Random.Range(1, 4);
        if (SpawnChange == 2)
        {
            GameObject OrbToSpawn = ElementOrbs[Random.Range(0, ElementOrbs.Length)];
            Instantiate(OrbToSpawn, transform.position, quaternion.identity);
        }
    }
}
