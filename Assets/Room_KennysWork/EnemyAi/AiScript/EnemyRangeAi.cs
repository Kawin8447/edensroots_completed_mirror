using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.TextCore.Text;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using Unity.Mathematics;

public class EnemyRangeAi : MonoBehaviour, Damageable
{
    public string PlayerObject;

    public NavMeshAgent agent;

    public Transform player;

    public LayerMask whatIsGround, whatIsPlayer;

    public float Health;
    private float Maxhealth;
    private float currentHealth;

    public Image HealthBar;
    public TextMeshProUGUI HealthText;

    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    //Attacking
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    public GameObject[] Projectiles;

    //States
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    public GameObject shooterPoint;

    public static event System.Action<bool> RangeRoomClear;

    [Header("Damage Indecator")]
    public Animator DamageAnim;
    public TextMeshProUGUI DamageText;
    private float TotalDamage;

    private string AiElement;
    private string PlayerElement;

    public Material[] Colors;

    private float FinalDamage;

    public LineRenderer lineRenderer;
    [SerializeField]private GameObject _body;
    [SerializeField]private Animator enemyAnim;

    private bool CanAttack = true;

    private void Awake()
    {
        player = GameObject.Find(PlayerObject).transform;
        agent = GetComponent<NavMeshAgent>();

        Maxhealth = Health;
        currentHealth = Health;

        DamageText.gameObject.SetActive(false);

        int random = Random.Range(1, 5);
        if (random == 1) { AiElement = "None"; }
        if (random == 2) { AiElement = "Water"; }
        if (random == 3) { AiElement = "Earth"; }
        if (random == 4) { AiElement = "Wind"; }

        lineRenderer.enabled = false;
    }

    private void Start()
    {
        if (AiElement == "Normal")
        {
            _body.GetComponent<SkinnedMeshRenderer>().material = Colors[0];
        }
        if (AiElement == "Water")
        {
            _body.GetComponent<SkinnedMeshRenderer>().material = Colors[1];
        }
        if (AiElement == "Earth")
        {
            _body.GetComponent<SkinnedMeshRenderer>().material = Colors[2];
        }
        if (AiElement == "Wind")
        {
            _body.GetComponent<SkinnedMeshRenderer>().material = Colors[3];
        }
    }

    private void Update()
    {
        //Check for sight and attack range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        if (!playerInSightRange && !playerInAttackRange) Patroling();
        if (playerInSightRange && !playerInAttackRange) ChasePlayer();
        if (playerInAttackRange && playerInSightRange) AttackPlayer();

        if (currentHealth != Health)
        {
            if (currentHealth > Health)
            {
                currentHealth--;
            }

            if (currentHealth < Health)
            {
                currentHealth++;
            }
        }

        HealthBar.fillAmount = currentHealth / Maxhealth;
        HealthText.text = Mathf.Round(currentHealth).ToString();

        PlayerElement = GameRoomManager.Instance._currentElementVar;

        DrawLine();
    }

    private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet && Health > 0)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    private void ChasePlayer()
    {
        if (Health > 0)
        {
            agent.SetDestination(player.position);
        }
    }

    private void AttackPlayer()
    {
        if (Health > 0)
        {
            agent.SetDestination(transform.position);

            transform.LookAt(player);

            if (!alreadyAttacked && CanAttack)
            {
                StartCoroutine(DrawLineToPlayerCoroutine());
            }
        }
    }

    private IEnumerator DrawLineToPlayerCoroutine()
    {
        CanAttack = false;
        lineRenderer.enabled = true;
        lineRenderer.positionCount = 2;

        yield return new WaitForSeconds(1f);

        lineRenderer.enabled = false;
        enemyAnim.SetTrigger("Attack");
        StartCoroutine(AttackAfterDelay(0f));
    }

    private IEnumerator AttackAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);

        // Attack code
        if (AiElement == "None")
        {
            Rigidbody rb = Instantiate(Projectiles[0], shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
        }
        if (AiElement == "Water")
        {
            Rigidbody rb = Instantiate(Projectiles[1], shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
        }
        if (AiElement == "Earth")
        {
            Rigidbody rb = Instantiate(Projectiles[2], shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
        }
        if (AiElement == "Wind")
        {
            Rigidbody rb = Instantiate(Projectiles[3], shooterPoint.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
        }

        alreadyAttacked = true;
        Invoke(nameof(ResetAttack), timeBetweenAttacks);
    }

    private void DrawLine()
    {
        lineRenderer.startWidth = 0.1f;
        lineRenderer.endWidth = 0.1f;
        lineRenderer.SetPosition(0, shooterPoint.transform.position + 0.5f * transform.forward);
        lineRenderer.SetPosition(1, player.position - 1f * transform.forward);
    }
    private void ResetAttack()
    {
        alreadyAttacked = false;
        CanAttack = true;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }

    private void DestryEnemy()
    {
        if (GameRoomManager.Instance.CurrentEnemyCount > 0)
        {
            GameRoomManager.Instance.CurrentEnemyCount -= 1;

            Debug.Log("Current Enemy = " + GameRoomManager.Instance.CurrentEnemyCount);

            if (GameRoomManager.Instance.CurrentEnemyCount == 0)
            {
                Debug.Log("Room Clear");
                RangeRoomClear?.Invoke(true);
            }
        }
        SpawnElement();
        Destroy(gameObject);
    }

    public void DoDamage(float damage)
    {
        FinalDamage = 0f;
        PlayerElement = GameRoomManager.Instance._currentElementVar;

        if (AiElement == "None")
        {
            if (PlayerElement == "None") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Water") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Earth") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Wind") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
        }
        else if (AiElement == "Water")
        {
            if (PlayerElement == "None") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Water") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Earth") { FinalDamage = (damage / 2); DamageText.color = Color.gray; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Wind") { FinalDamage = (damage * 2); DamageText.color = Color.red; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
        }
        else if (AiElement == "Earth")
        {
            if (PlayerElement == "None") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Water") { FinalDamage = (damage * 2); DamageText.color = Color.red; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Earth") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Wind") { FinalDamage = (damage / 2); DamageText.color = Color.gray; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
        }
        else if (AiElement == "Wind")
        {
            if (PlayerElement == "None") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Water") { FinalDamage = (damage / 2); DamageText.color = Color.gray; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Earth") { FinalDamage = (damage * 2); DamageText.color = Color.red; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
            else if (PlayerElement == "Wind") { FinalDamage = damage; DamageText.color = Color.white; DamageText.gameObject.SetActive(true); SetupDamage(FinalDamage); }
        }
    }
    private void SetupDamage(float ShowUpDamage)
    {
        Health -= ShowUpDamage;

        Debug.Log("Ai take damage by bullet HP = " + Health);
        if (Health <= 0)
        {
            agent.enabled = false;
            Invoke(nameof(DestryEnemy), 0.5f);
        }

        if (DamageAnim.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.33f)
        {
            TotalDamage += ShowUpDamage;
            DamageText.text = TotalDamage.ToString();
            DamageAnim.SetTrigger("GetAnDamage");
        }
        else
        {
            TotalDamage = ShowUpDamage;
            DamageText.text = TotalDamage.ToString();
            DamageAnim.SetTrigger("GetAnDamage");
        }
    }
    [Header("Element Orbs")]
    public GameObject[] ElementOrbs;
    private void SpawnElement()
    {
        int SpawnChange = Random.Range(1, 4);
        if (SpawnChange == 2)
        {
            GameObject OrbToSpawn = ElementOrbs[Random.Range(0, ElementOrbs.Length)];
            Instantiate(OrbToSpawn, transform.position, quaternion.identity);
        }
    }
}
