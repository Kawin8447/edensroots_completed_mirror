using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    [SerializeField] private float lifetime = 1f;
    public GameObject explosion;

    public float damageAmount = 10;

    public enum ElementType
    {
        None,
        Water,
        Earth,
        Wind
    }

    public ElementType AttackElement;

    public Material[] Colors;

    private void Start()
    {
        Invoke("DestroyBullet", lifetime);
    }

    private void OnTriggerEnter(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable != null && !other.gameObject.CompareTag("Enemy"))
        {
            switch (AttackElement)
            {
                case ElementType.None:
                    GetComponent<MeshRenderer>().material = Colors[0];
                    if (GameRoomManager.Instance._currentElementVar == "None"
                    || GameRoomManager.Instance._currentElementVar == "Water"
                    || GameRoomManager.Instance._currentElementVar == "Earth"
                    || GameRoomManager.Instance._currentElementVar == "Wind")
                    {
                        damageable.DoDamage(damageAmount);
                    }
                    break;
                case ElementType.Water:
                    GetComponent<MeshRenderer>().material = Colors[1];
                    if (GameRoomManager.Instance._currentElementVar == "Earth")
                    {
                        damageable.DoDamage(damageAmount + (damageAmount * 0.5f));
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Wind")
                    {
                        damageable.DoDamage(damageAmount / 2);
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Water"
                    || GameRoomManager.Instance._currentElementVar == "None")
                    {
                        damageable.DoDamage(damageAmount);
                    }
                    break;
                case ElementType.Earth:
                    GetComponent<MeshRenderer>().material = Colors[2];
                    if (GameRoomManager.Instance._currentElementVar == "Wind")
                    {
                        damageable.DoDamage(damageAmount + (damageAmount * 0.5f));
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Water")
                    {
                        damageable.DoDamage(damageAmount / 2);
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Earth"
                    || GameRoomManager.Instance._currentElementVar == "None")
                    {
                        damageable.DoDamage(damageAmount);
                    }
                    break;
                case ElementType.Wind:
                    GetComponent<MeshRenderer>().material = Colors[3];
                    if (GameRoomManager.Instance._currentElementVar == "Water")
                    {
                        damageable.DoDamage(damageAmount + (damageAmount * 0.5f));
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Earth")
                    {
                        damageable.DoDamage(damageAmount / 2);
                    }
                    if (GameRoomManager.Instance._currentElementVar == "Wind"
                    || GameRoomManager.Instance._currentElementVar == "None")
                    {
                        damageable.DoDamage(damageAmount);
                    }
                    break;
            }


            if (explosion != null)
                Instantiate(explosion, transform.position, Quaternion.identity);

            Destroy(gameObject);
        }
    }

    private void DestroyBullet()
    {
        if (explosion != null)
            Instantiate(explosion, transform.position, Quaternion.identity);

        Destroy(gameObject);
    }
}