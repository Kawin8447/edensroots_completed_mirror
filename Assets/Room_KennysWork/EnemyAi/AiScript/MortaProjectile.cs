using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using UnityEngine;

public class MortaProjectile : MonoBehaviour
{
    public GameObject Ball;
    public GameObject Liquid;

    private bool Falling = true;

    private bool Stay = false;

    public BoxCollider boxCollider;

    void Awake()
    {
        Liquid.gameObject.SetActive(false);

        boxCollider.enabled = false;
    }
    void Start()
    {
        transform.position = new Vector3(transform.position.x, 15, transform.position.z);
    }

    void FixedUpdate()
    {
        if (Falling) { transform.Translate(Vector3.down * (Time.deltaTime * 4f)); }

        if (transform.position.y <= 1.6)
        {
            Falling = false;
            Ball.gameObject.SetActive(false);
            Liquid.gameObject.SetActive(true);
            boxCollider.enabled = true;

            StartCoroutine(LefeTime());
        }
    }

    IEnumerator LefeTime()
    {

        yield return new WaitForSeconds(60f);

        Destroy(gameObject);
    }

    private void OnTriggerStay(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable != null && !other.gameObject.CompareTag("Enemy") && Stay == false)
        {
            Stay = true;
            damageable.DoDamage(5);
            StartCoroutine(ResetStay());
        }
    }

    IEnumerator ResetStay()
    {

        yield return new WaitForSeconds(1f);

        Stay = false;
    }
}
