using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class MinimapManager : MonoBehaviour
{
    public static MinimapManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    public RawImage Minimap;
    public RenderTexture MinimapTexture;
    public RenderTexture TrackingTexture;

    public bool playerIsInField = false;
    public bool playerIsInOtherRoom = true;

    public TextMeshProUGUI CurrentFloorText;

    void LateUpdate()
    {
        if (playerIsInField && !playerIsInOtherRoom)
        {
            Minimap.texture = MinimapTexture;
        }
        else if (playerIsInOtherRoom && !playerIsInField)
        {
            Minimap.texture = TrackingTexture;
        }

        CurrentFloorText.text = "Floor: " + GameRoomManager.Instance.Floor.ToString();
    }
}
