using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;

public class RoomTriggerCheck : MonoBehaviour
{
    public static event System.Action<string> PlayerIsOnTheRoom;
    
    public enum PlayerOnRoom
    {
        Room1,
        Room2,
        Room3,
        Room4,
        Room5,
        Room6,
        Room7,
        StartRoom,
        FinishRoom,
        BossRoom
    }
    
    public PlayerOnRoom PlayerIsOnRoom;
    
    [HideInInspector]
    [SerializeField] private string RoomName;
    
    private void Start()
    {
        switch (PlayerIsOnRoom)
        {
            case PlayerOnRoom.Room1:
                RoomName = "PlayerIsOnRoom1";
                break;
            case PlayerOnRoom.Room2:
                RoomName = "PlayerIsOnRoom2";
                break;
            case PlayerOnRoom.Room3:
                RoomName = "PlayerIsOnRoom3";
                break;
            case PlayerOnRoom.Room4:
                RoomName = "PlayerIsOnRoom4";
                break;
            case PlayerOnRoom.Room5:
                RoomName = "PlayerIsOnRoom5";
                break;
            case PlayerOnRoom.Room6:
                RoomName = "PlayerIsOnRoom6";
                break;
            case PlayerOnRoom.Room7:
                RoomName = "PlayerIsOnRoom7";
                break;
            case PlayerOnRoom.StartRoom:
                RoomName = "PlayerIsOnStartRoom";
                break;
            case PlayerOnRoom.FinishRoom:
                RoomName = "PlayerIsOnFinishRoom";
                break;
            case PlayerOnRoom.BossRoom:
                RoomName = "PlayerIsOnBossRoom";
                break;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerIsOnTheRoom?.Invoke(RoomName);
        }
    }
}
