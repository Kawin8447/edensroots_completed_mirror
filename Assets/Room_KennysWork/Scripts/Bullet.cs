using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float lifetime = 3f;
    
    public GameObject explosion;
    
    public int damageAmount = 10;


    private void Start()
    {
        Invoke("DestroyBullet", lifetime);
    }

    private void OnTriggerEnter(Collider other)
    {
        Damageable damageable = other.GetComponent<Damageable>();

        if (damageable != null && !other.gameObject.CompareTag("Player"))
        {
            damageable.DoDamage(damageAmount);
            
            Destroy(gameObject);
        }
    }

    private void DestroyBullet()
    {
        Destroy(gameObject);
        if (explosion != null) Instantiate(explosion, transform.position, Quaternion.identity);
    }
}