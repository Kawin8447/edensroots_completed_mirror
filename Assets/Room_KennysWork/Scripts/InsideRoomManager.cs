using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Photon.Realtime;
using TMPro;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEditor.Rendering;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class InsideRoomManager : PureSingleton<InsideRoomManager>
{
    [Header("Enemy prefab")]
    public GameObject[] EnemyPrefab;

    [Header("Ai spawn point")]
    public GameObject[] EachAiSpawnpoint;

    [Header("Each Puzzel type")]
    public GameObject[] puzzelType;

    [Header("Center of each room")]
    public GameObject[] CenterOfRoom;

    [Header("Objective Displayer")]
    public TextMeshProUGUI _currentEnemyText;
    public GameObject EnemyStatusCard;
    public GameObject PuzzelStatusCard;

    private List<int> previousRandomNumbers = new List<int>();
    private const int MAX_DUPLICATES = 2;
    private const int MIN_RANDOM_NUMBER = 1;
    private const int MAX_RANDOM_NUMBER = 2;

    public int[] typeRoom;
    [SerializeField] private UnityEngine.Rendering.Volume volume;
    public float increaseDuration = 2f; // Duration in seconds for the value to increase from 0 to 1
    private float currentValue;
    private AudioSwitcher _switcher;

    private Image[] FightRoomIcon;
    private Image[] PuzzelRoomIcon;

    [Header("Boss Campaign")]
    public GameObject BossSpawnPoint;
    public GameObject BossPrefab;
    private bool firstTimeBossRoom = true;

    [Header("RoomStats")]
    public RoomStat _roomStats;
    public GameObject RoomClearText;
    public GameObject CampaignClearText;
    public TextMeshProUGUI moneyText;

    private void OnEnable()
    {
        RoomTriggerCheck.PlayerIsOnTheRoom += PlayerEnterRoom;

        EnemyRangeAi.RangeRoomClear += RoomIsClear;
        EnemyMeleeAi.MeleeRoomClear += RoomIsClear;
        _PuzzelTest.PuzzelRoomClear += RoomIsClear;

        EnemyBossAi.BossRoomClear += CampaignClear;
    }

    private void OnDisable()
    {
        RoomTriggerCheck.PlayerIsOnTheRoom -= PlayerEnterRoom;

        EnemyRangeAi.RangeRoomClear -= RoomIsClear;
        EnemyMeleeAi.MeleeRoomClear -= RoomIsClear;
        _PuzzelTest.PuzzelRoomClear -= RoomIsClear;

        EnemyBossAi.BossRoomClear -= CampaignClear;
    }

    private new void Awake()
    {
        RandomNewRoomType();
        typeRoom[0] = 2;
    }

    private void Start()
    {
        EnemyStatusCard.SetActive(false);
        PuzzelStatusCard.SetActive(false);
        _switcher = GetComponent<AudioSwitcher>();
        UpdateIcon();

        RoomClearText.gameObject.SetActive(false);
        CampaignClearText.gameObject.SetActive(false);
    }

    public void RandomNewRoomType()
    {
        Debug.LogWarning("New Room Type Just Random");
        typeRoom[0] = Random.Range(1, 7);
        typeRoom[1] = Random.Range(1, 7);
        typeRoom[2] = Random.Range(1, 7);
        typeRoom[3] = Random.Range(1, 7);
        typeRoom[4] = Random.Range(1, 7);
        typeRoom[5] = Random.Range(1, 7);
        typeRoom[6] = Random.Range(1, 7);
    }

    public void UpdateIcon()
    {
        Debug.LogWarning("Update icon");
    }

    private void Update()
    {
        _currentEnemyText.text = "Enemy Remaining: " + GameRoomManager.Instance.CurrentEnemyCount;
        moneyText.SetText("Money"+" : " + _roomStats.money);
        volume.weight = currentValue;
    }

    public void SpawnEnemy(int room)
    {
        for (int i = 0; i < GameRoomManager.Instance.EnemySpawn; i++)
        {
            GameObject AmountPrefab = EnemyPrefab[Random.Range(0, EnemyPrefab.Length)];

            float spawnRadius = 15f;
            float randomAngle = Random.Range(0f, 2f * Mathf.PI);

            float newSpawnPosX = EachAiSpawnpoint[room].transform.position.x + spawnRadius * Mathf.Cos(randomAngle);
            float newSpawnPosZ = EachAiSpawnpoint[room].transform.position.z + spawnRadius * Mathf.Sin(randomAngle);

            Vector3 SpawnPos = new Vector3(newSpawnPosX, EachAiSpawnpoint[room].transform.position.y, newSpawnPosZ);

            Instantiate(AmountPrefab, SpawnPos, quaternion.identity);
        }
    }

    private void PlayerEnterRoom(string roomName)
    {
        if (roomName == "PlayerIsOnRoom1")
        {
            Debug.Log("Player has enter room1");
            if (GameRoomManager.Instance.Room1Clear == false && GameRoomManager.Instance.FristTimeEnterRoom1 == true)
            {
                StartCoroutine(IncreaseValue());
                _switcher.SwitchSound();
                GameRoomManager.Instance.FristTimeEnterRoom1 = false;
                //ปิดประตู
                GameRoomManager.Instance.GateRoom[0].SetActive(true);

                //ห้องสู้
                if (typeRoom[0] == 2 || typeRoom[0] == 3 || typeRoom[0] == 4 || typeRoom[0] == 5 || typeRoom[0] == 6)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.EnemySpawn;

                    SpawnEnemy(0);
                }
                //ห้องปริศนา
                if (typeRoom[0] == 1)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[0].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom2")
        {
            Debug.Log("Player has enter room2");

            if (GameRoomManager.Instance.Room2Clear == false && GameRoomManager.Instance.FristTimeEnterRoom2 == true)
            {
                StartCoroutine(IncreaseValue());
                _switcher.SwitchSound();
                GameRoomManager.Instance.FristTimeEnterRoom2 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[1].SetActive(true);

                //ห้องสู้
                if (typeRoom[1] == 2 || typeRoom[1] == 3 || typeRoom[1] == 4 || typeRoom[1] == 5 || typeRoom[1] == 6)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.EnemySpawn;

                    SpawnEnemy(1);
                }
                //ห้องปริศนา
                if (typeRoom[1] == 1)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[1].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom3")
        {
            Debug.Log("Player has enter room3");

            if (GameRoomManager.Instance.Room3Clear == false && GameRoomManager.Instance.FristTimeEnterRoom3 == true)
            {
                StartCoroutine(IncreaseValue());
                _switcher.SwitchSound();
                GameRoomManager.Instance.FristTimeEnterRoom3 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[2].SetActive(true);

                //ห้องสู้
                if (typeRoom[2] == 2 || typeRoom[2] == 3 || typeRoom[2] == 4 || typeRoom[2] == 5 || typeRoom[2] == 6)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.EnemySpawn;

                    SpawnEnemy(2);
                }
                //ห้องปริศนา
                if (typeRoom[2] == 1)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[2].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom4")
        {
            Debug.Log("Player has enter room4");

            if (GameRoomManager.Instance.Room4Clear == false && GameRoomManager.Instance.FristTimeEnterRoom4 == true)
            {
                StartCoroutine(IncreaseValue());
                _switcher.SwitchSound();
                GameRoomManager.Instance.FristTimeEnterRoom4 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[3].SetActive(true);

                //ห้องสู้
                if (typeRoom[3] == 2 || typeRoom[3] == 3 || typeRoom[3] == 4 || typeRoom[3] == 5 || typeRoom[3] == 6)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.EnemySpawn;

                    SpawnEnemy(3);
                }
                //ห้องปริศนา
                if (typeRoom[3] == 1)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[3].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom5")
        {
            Debug.Log("Player has enter room5");

            if (GameRoomManager.Instance.Room5Clear == false && GameRoomManager.Instance.FristTimeEnterRoom5 == true)
            {
                StartCoroutine(IncreaseValue());
                _switcher.SwitchSound();
                GameRoomManager.Instance.FristTimeEnterRoom5 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[4].SetActive(true);

                //ห้องสู้
                if (typeRoom[4] == 2 || typeRoom[4] == 3 || typeRoom[4] == 4 || typeRoom[4] == 5 || typeRoom[4] == 6)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.EnemySpawn;

                    SpawnEnemy(4);
                }
                //ห้องปริศนา
                if (typeRoom[4] == 1)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[4].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom6")
        {
            Debug.Log("Player has enter room6");

            if (GameRoomManager.Instance.Room6Clear == false && GameRoomManager.Instance.FristTimeEnterRoom6 == true)
            {
                StartCoroutine(IncreaseValue());
                _switcher.SwitchSound();
                GameRoomManager.Instance.FristTimeEnterRoom6 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[5].SetActive(true);

                //ห้องสู้
                if (typeRoom[5] == 2 || typeRoom[5] == 3 || typeRoom[5] == 4 || typeRoom[5] == 5 || typeRoom[5] == 6)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.EnemySpawn;

                    SpawnEnemy(5);
                }
                //ห้องปริศนา
                if (typeRoom[5] == 1)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[5].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnRoom7")
        {
            Debug.Log("Player has enter room7");

            if (GameRoomManager.Instance.Room7Clear == false && GameRoomManager.Instance.FristTimeEnterRoom7 == true)
            {
                StartCoroutine(IncreaseValue());
                _switcher.SwitchSound();
                GameRoomManager.Instance.FristTimeEnterRoom7 = false;

                //ปิดประตู
                GameRoomManager.Instance.GateRoom[6].SetActive(true);

                //ห้องสู้
                if (typeRoom[6] == 2 || typeRoom[6] == 3 || typeRoom[6] == 5 || typeRoom[6] == 6)
                {
                    Debug.LogWarning("Enemy spawned");
                    EnemyStatusCard.SetActive(true);
                    GameRoomManager.Instance.CurrentEnemyCount = GameRoomManager.Instance.EnemySpawn;

                    SpawnEnemy(6);
                }
                //ห้องปริศนา
                if (typeRoom[6] == 1 || typeRoom[6] == 4)
                {
                    Debug.LogWarning("Puzzel spawned");
                    PuzzelStatusCard.SetActive(true);
                    int randPuzzelType = Random.Range(0, 3);
                    Instantiate(puzzelType[randPuzzelType], CenterOfRoom[6].transform.position, quaternion.identity);
                }
            }
        }

        if (roomName == "PlayerIsOnBossRoom")
        {
            if (firstTimeBossRoom == true)
            {
                firstTimeBossRoom = false;
                Invoke(nameof(SpawnBoss), 4f);
            }
            Debug.LogWarning("Player has Enter Boss Room");
        }
    }

    private void SpawnBoss()
    {
        Instantiate(BossPrefab, BossSpawnPoint.transform.position, quaternion.identity);
    }

    private void RoomIsClear(bool RoomClear)
    {
        if (RoomClear == true && GameRoomManager.Instance.Room1Clear == false && GameRoomManager.Instance.FristTimeEnterRoom1 == false)
        {
            StartCoroutine(DecreaseValue());
            _switcher.SwitchSound();
            GameRoomManager.Instance.Room1Clear = true;
            GameRoomManager.Instance.GateRoom[0].SetActive(false);
            volume.weight = 0;

            ClearObjectivePanel();
            if (typeRoom[0] == 2 || typeRoom[0] == 3 || typeRoom[0] == 4 || typeRoom[0] == 5 || typeRoom[0] == 6)
            {
                HealPlayer(10);
            }
            else
            {
                _roomStats.money += 10;
            }

            GameRoomManager.Instance.marker[0].SetActive(true);
            StartCoroutine(ShowRoomClearText());

            GameRoomManager.Instance.IncreaseEnemy();

            //Reward
            _roomStats.money += 5;
            _roomStats.enemyRoomClear++;
        }

        if (RoomClear == true && GameRoomManager.Instance.Room2Clear == false && GameRoomManager.Instance.FristTimeEnterRoom2 == false)
        {
            StartCoroutine(DecreaseValue());
            _switcher.SwitchSound();
            GameRoomManager.Instance.Room2Clear = true;
            GameRoomManager.Instance.GateRoom[1].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[1] == 2 || typeRoom[1] == 3 || typeRoom[1] == 4 || typeRoom[1] == 5 || typeRoom[1] == 6)
            {
                HealPlayer(10);
            }else
            {
                _roomStats.money += 10;
            }

            GameRoomManager.Instance.marker[1].SetActive(true);
            StartCoroutine(ShowRoomClearText());

            GameRoomManager.Instance.IncreaseEnemy();
            _roomStats.money += 5;
            _roomStats.enemyRoomClear++;
        }

        if (RoomClear == true && GameRoomManager.Instance.Room3Clear == false && GameRoomManager.Instance.FristTimeEnterRoom3 == false)
        {
            StartCoroutine(DecreaseValue());
            _switcher.SwitchSound();
            GameRoomManager.Instance.Room3Clear = true;
            GameRoomManager.Instance.GateRoom[2].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[2] == 2 || typeRoom[2] == 3 || typeRoom[2] == 4 || typeRoom[2] == 5 || typeRoom[2] == 6)
            {
                HealPlayer(10);
            }else
            {
                _roomStats.money += 10;
            }

            GameRoomManager.Instance.marker[2].SetActive(true);
            StartCoroutine(ShowRoomClearText());

            GameRoomManager.Instance.IncreaseEnemy();
            _roomStats.money += 5;
            _roomStats.enemyRoomClear++;
        }

        if (RoomClear == true && GameRoomManager.Instance.Room4Clear == false && GameRoomManager.Instance.FristTimeEnterRoom4 == false)
        {
            StartCoroutine(DecreaseValue());
            _switcher.SwitchSound();
            GameRoomManager.Instance.Room4Clear = true;
            GameRoomManager.Instance.GateRoom[3].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[3] == 2 || typeRoom[3] == 3 || typeRoom[3] == 4 || typeRoom[3] == 5 || typeRoom[3] == 6)
            {
                HealPlayer(10);
            }else
            {
                _roomStats.money += 10;
            }

            GameRoomManager.Instance.marker[3].SetActive(true);
            StartCoroutine(ShowRoomClearText());

            GameRoomManager.Instance.IncreaseEnemy();
            _roomStats.money += 5;
            _roomStats.enemyRoomClear++;
        }

        if (RoomClear == true && GameRoomManager.Instance.Room5Clear == false && GameRoomManager.Instance.FristTimeEnterRoom5 == false)
        {
            StartCoroutine(DecreaseValue());
            _switcher.SwitchSound();
            GameRoomManager.Instance.Room5Clear = true;
            GameRoomManager.Instance.GateRoom[4].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[4] == 2 || typeRoom[4] == 3 || typeRoom[4] == 4 || typeRoom[4] == 5 || typeRoom[4] == 6)
            {
                HealPlayer(10);
            }else
            {
                _roomStats.money += 10;
            }

            GameRoomManager.Instance.marker[4].SetActive(true);
            StartCoroutine(ShowRoomClearText());

            GameRoomManager.Instance.IncreaseEnemy();
            _roomStats.money += 5;
            _roomStats.enemyRoomClear++;
        }

        if (RoomClear == true && GameRoomManager.Instance.Room6Clear == false && GameRoomManager.Instance.FristTimeEnterRoom6 == false)
        {
            StartCoroutine(DecreaseValue());
            _switcher.SwitchSound();
            GameRoomManager.Instance.Room6Clear = true;
            GameRoomManager.Instance.GateRoom[5].SetActive(false);

            ClearObjectivePanel();
            if (typeRoom[5] == 2 || typeRoom[5] == 3 || typeRoom[5] == 4 || typeRoom[5] == 5 || typeRoom[5] == 6)
            {
                HealPlayer(10);
            }else
            {
                _roomStats.money += 10;
            }

            GameRoomManager.Instance.marker[5].SetActive(true);
            StartCoroutine(ShowRoomClearText());

            GameRoomManager.Instance.IncreaseEnemy();
            _roomStats.money += 5;
            _roomStats.enemyRoomClear++;
        }

        if (RoomClear == true && GameRoomManager.Instance.Room7Clear == false && GameRoomManager.Instance.FristTimeEnterRoom7 == false)
        {
            StartCoroutine(DecreaseValue());
            _switcher.SwitchSound();
            GameRoomManager.Instance.Room7Clear = true;
            GameRoomManager.Instance.GateRoom[6].SetActive(false);
            
            ClearObjectivePanel();
            if (typeRoom[6] == 2 || typeRoom[6] == 3 || typeRoom[6] == 5 || typeRoom[6] == 6)
            {
                HealPlayer(10);
            }else
            {
                _roomStats.money += 10;
            }

            GameRoomManager.Instance.marker[6].SetActive(true);
            StartCoroutine(ShowRoomClearText());

            GameRoomManager.Instance.IncreaseEnemy();
            _roomStats.money += 5;
            _roomStats.enemyRoomClear++;
        }
    }

    private void ClearObjectivePanel()
    {
        if (EnemyStatusCard)
        {
            EnemyStatusCard.SetActive(false);
        }
        if (PuzzelStatusCard)
        {
            PuzzelStatusCard.SetActive(false);
        }
    }

    public void HealPlayer(int Amount)
    {
        GameObject _player = GameObject.Find("PlayerManage(Clone)");

        if (_player != null)
        {
            PlayerManager _playerManager = _player.GetComponent<PlayerManager>();
            if (_playerManager != null)
            {
                _playerManager.HealPlayer(10);
            }
        }
    }

    public void CampaignClear(bool clear)
    {
        if (clear == true)
        {
            GameObject[] AliveAi = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject gameObjectWithTag in AliveAi)
            {
                Destroy(gameObjectWithTag);
            }


            HealPlayer(100);

            StartCoroutine(ShowCampaignClearText());
            Invoke(nameof(CallClearCampaign), 3.5f);
        }

    }
    private void CallClearCampaign()
    {
        Debug.LogWarning("Campaign has been clear");
        GameRoomManager.Instance.CampaignClear();
    }
    public IEnumerator ShowRoomClearText()
    {
        yield return new WaitForSeconds(0.25f);

        RoomClearText.gameObject.SetActive(true);

        yield return new WaitForSeconds(3f);

        RoomClearText.gameObject.SetActive(false);
    }

    public IEnumerator ShowCampaignClearText()
    {
        yield return new WaitForSeconds(0.25f);

        CampaignClearText.gameObject.SetActive(true);

        yield return new WaitForSeconds(3f);

        CampaignClearText.gameObject.SetActive(false);
    }

    private IEnumerator IncreaseValue()
    {
        float startTime = Time.time;
        float endTime = startTime + increaseDuration;

        while (Time.time < endTime)
        {
            float progress = (Time.time - startTime) / increaseDuration;
            currentValue = Mathf.Lerp(0f, 1f, progress);
            yield return null;
        }
        currentValue = 1f;
    }
    private IEnumerator DecreaseValue()
    {
        float startTime = Time.time;
        float endTime = startTime + increaseDuration;

        while (Time.time < endTime)
        {
            float progress = (Time.time - startTime) / increaseDuration;
            currentValue = Mathf.Lerp(1f, 0f, progress);
            yield return null;
        }
        currentValue = 0f;
    }
}
