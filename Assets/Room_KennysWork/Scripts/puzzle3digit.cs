using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class puzzle3digit : MonoBehaviour
{
    private _PuzzelTest setNum;
    private ParticleSystem _particleSystem;
    public Puzzle3object[] _puzzle3Object;
    [Header("Random")]
    public int digit1, digit2, digit3, digit4;
    [Header("Player input")]
    public int pdigit1, pdigit2, pdigit3, pdigit4;

    private void Awake()
    {
        setNum = GetComponentInParent<_PuzzelTest>();
        _puzzle3Object = GetComponentsInChildren<Puzzle3object>();
        _particleSystem = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        digit1 = _puzzle3Object[0].digit;
        digit2 = _puzzle3Object[1].digit;
        digit3 = _puzzle3Object[2].digit;
        digit4 = _puzzle3Object[3].digit;
        pdigit1 = _puzzle3Object[0]._pInput;
        pdigit2 = _puzzle3Object[1]._pInput;
        pdigit3 = _puzzle3Object[2]._pInput;
        pdigit4 = _puzzle3Object[3]._pInput;
        if (pdigit1 != 0 && pdigit2 != 0 && pdigit3 != 0 && pdigit4 != 0)
        {
            if (digit1 == pdigit1 && digit2 == pdigit2 && digit3 == pdigit3 && digit4 == pdigit4)
            {
                StartCoroutine(particle());
            }
            else
            {
                _puzzle3Object[0].resetColor();
                _puzzle3Object[1].resetColor();
                _puzzle3Object[2].resetColor();
                _puzzle3Object[3].resetColor();
            }
        }
    }

    IEnumerator particle()
    {
        _particleSystem.Play();
        yield return new WaitForSeconds(0.3f);
        setNum.blockLeft--;
    }
}
