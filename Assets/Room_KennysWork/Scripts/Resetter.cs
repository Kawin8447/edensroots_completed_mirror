using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.InputSystem;

public class Resetter : MonoBehaviour
{
    public GameObject spawmPoint;
    private GameObject target;
    private GameObject player;

    public int HealingAmount;

    public GameObject BossRoomTarget;

    public GameObject ChoosePanel;
    public GameObject FinishRoomLocation;

    private void Awake()
    {
        GameRoomManager.Instance.Room1Clear = false;
        GameRoomManager.Instance.Room2Clear = false;
        GameRoomManager.Instance.Room3Clear = false;
        GameRoomManager.Instance.Room4Clear = false;
        GameRoomManager.Instance.Room5Clear = false;
        GameRoomManager.Instance.Room6Clear = false;
        GameRoomManager.Instance.Room7Clear = false;

        GameRoomManager.Instance.FristTimeEnterRoom1 = true;
        GameRoomManager.Instance.FristTimeEnterRoom2 = true;
        GameRoomManager.Instance.FristTimeEnterRoom3 = true;
        GameRoomManager.Instance.FristTimeEnterRoom4 = true;
        GameRoomManager.Instance.FristTimeEnterRoom5 = true;
        GameRoomManager.Instance.FristTimeEnterRoom6 = true;
        GameRoomManager.Instance.FristTimeEnterRoom7 = true;

        GameRoomManager.Instance.CurrentEnemyCount = 0;

        GameRoomManager.Instance.Floor = 1;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            GameObject _player = GameObject.Find("PlayerManage(Clone)");

            if (_player != null)
            {
                _player.transform.position = FinishRoomLocation.transform.position;
            }
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            GameObject _player = GameObject.Find("PlayerManage(Clone)");

            if (_player != null)
            {
                _player.transform.position = BossRoomTarget.transform.position;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log(" Player Completed the Floor");

            GameObject _player = GameObject.Find("PlayerManage(Clone)");

            if (_player != null)
            {
                PlayerManager _playerManager = _player.GetComponent<PlayerManager>();
                if (_playerManager != null)
                {
                    _playerManager.HealPlayer(HealingAmount);
                }
            }

            target = other.gameObject;
            player = target.transform.GetChild(1).gameObject;
            player.GetComponent<CharacterController>().enabled = false;
            GameRoomManager.Instance.Transition.SetTrigger("Start");

            //Invoke(nameof(NormalClear), 1.5f);

            if (GameRoomManager.Instance.Floor >= 3)
            {
                Invoke(nameof(CallChoosePanel), 1f);
            }
            else
            {
                Invoke(nameof(NormalReset), 1.5f);
            }

        }
    }
    public void NormalReset()
    {
        if (GameRoomManager.Instance.Floor >= 3)
        {
            CallChoosePanel();
        }
        GameRoomManager.Instance.Transition.SetTrigger("End");

        GameRoomManager.Instance.Room1Clear = false;
        GameRoomManager.Instance.Room2Clear = false;
        GameRoomManager.Instance.Room3Clear = false;
        GameRoomManager.Instance.Room4Clear = false;
        GameRoomManager.Instance.Room5Clear = false;
        GameRoomManager.Instance.Room6Clear = false;
        GameRoomManager.Instance.Room7Clear = false;

        GameRoomManager.Instance.FristTimeEnterRoom1 = true;
        GameRoomManager.Instance.FristTimeEnterRoom2 = true;
        GameRoomManager.Instance.FristTimeEnterRoom3 = true;
        GameRoomManager.Instance.FristTimeEnterRoom4 = true;
        GameRoomManager.Instance.FristTimeEnterRoom5 = true;
        GameRoomManager.Instance.FristTimeEnterRoom6 = true;
        GameRoomManager.Instance.FristTimeEnterRoom7 = true;

        player.transform.position = spawmPoint.transform.position;
        player.GetComponent<CharacterController>().enabled = true;

        GameRoomManager.Instance.Floor += 1;

        InsideRoomManager.Instance.RandomNewRoomType();

        InsideRoomManager.Instance.UpdateIcon();

        GameRoomManager.Instance.HideMarker();
    }

    public void CampaignReset()
    {
        //go to boss room
        CallChoosePanel();
        GameRoomManager.Instance.Transition.SetTrigger("End");
        player.transform.position = BossRoomTarget.transform.position;
        player.GetComponent<CharacterController>().enabled = true;

        MinimapManager.Instance.playerIsInField = false;
        MinimapManager.Instance.playerIsInOtherRoom = true;
    }

    public void CallChoosePanel()
    {
        ChoosePanel.SetActive(!ChoosePanel.activeSelf);
    }
}