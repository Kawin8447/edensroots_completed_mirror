using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SimpleLookToMouse : MonoBehaviour
{
    private Vector2 _mousePos;
    [SerializeField] private Camera camera1;

    private void Update()
    {
        RotateToMouse();
    }

    private void RotateToMouse()
    {
        Ray ray = camera1.ScreenPointToRay(_mousePos);

        if (Physics.Raycast(ray, out RaycastHit hitInfo, maxDistance: 500f))
        {
            var target = hitInfo.point;
            target.y = transform.position.y;

            // Calculate the direction from the player to the target
            Vector3 direction = target - transform.position;

            // Use Quaternion.LookRotation to set the rotation based on the direction
            transform.rotation = Quaternion.LookRotation(direction);
        }
    }

    public void MousePosition(InputAction.CallbackContext context)
    {
        _mousePos = context.action.ReadValue<Vector2>();
    }
}
