using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningUi : MonoBehaviour
{
    public GameObject WarningPanel;

    private GameObject target;
    private GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && GameRoomManager.Instance.Room7Clear == true)
        {
            OpenWarning();
        }
    }

    private void OpenWarning()
    {
        if (WarningPanel != null)
        {
            WarningPanel.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void CloseWarning()
    {
        WarningPanel.SetActive(false);
        Time.timeScale = 1f;
    }

    public void GoToFinishRoom()
    {
        Teleport.instance.TpRoom(Teleport.instance.FinishRoomSpot);
        MinimapManager.Instance.playerIsInField = false;
        MinimapManager.Instance.playerIsInOtherRoom = true;
        CloseWarning();
    }
}