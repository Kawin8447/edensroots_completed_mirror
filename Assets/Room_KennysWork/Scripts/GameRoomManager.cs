using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameRoomManager : PureSingleton<GameRoomManager>
{
    private new void Awake()
    {
        ResetDefault();

        HideMarker();

        Transition.SetTrigger("End");
    }

    [Header("Player Spawn Point")]
    public GameObject Player;
    public GameObject PlayerSpawnPoint;

    [Header("-Check clear status of each room")]
    public bool Room1Clear;
    public bool Room2Clear;
    public bool Room3Clear;
    public bool Room4Clear;
    public bool Room5Clear;
    public bool Room6Clear;
    public bool Room7Clear;

    [Header("-Check if each room can clear")]
    public bool FristTimeEnterRoom1;
    public bool FristTimeEnterRoom2;
    public bool FristTimeEnterRoom3;
    public bool FristTimeEnterRoom4;
    public bool FristTimeEnterRoom5;
    public bool FristTimeEnterRoom6;
    public bool FristTimeEnterRoom7;

    [Header("-Gates in each room")]
    public GameObject[] GateRoom;

    [Header("-Enemy inside each room")]
    public int EnemySpawn = 1;
    public float NextSpawn = 0;
    public float SpawnInterval = 0.5f;

    [Header("-Current Enemy inside current room")]
    public int CurrentEnemyCount = 0;

    [HideInInspector] public int Floor = 1;

    [Header("Transition")]
    public Animator Transition;

    [Header("About Reset game")]
    public GameObject GameoverPanel;
    public TextMeshProUGUI TotalFloortext;
    public TextMeshProUGUI GameoverHeaderText;

    [Header("About Clear game")]
    public GameObject ClearPanel;
    public TextMeshProUGUI TotalFloortext_ClearPanel;

    [Header("Text show Current element")]
    public TextMeshProUGUI _currentElementText;
    public string _currentElementVar;

    [Header("PauseMenu")] public GameObject pauseMenu;

    [Header("Timer Text")]
    public TextMeshProUGUI[] timerText;
    private float elapsedTime = 0f;
    private int minutes;
    private int seconds;

    [Header("Marker Of Each Room")]
    public GameObject[] marker;

    private void Update()
    {
        CallTimeUpdate();
        elapsedTime += Time.deltaTime;
        minutes = Mathf.FloorToInt(elapsedTime / 60f);
        seconds = Mathf.FloorToInt(elapsedTime % 60f);

        _currentElementText.text = "Element: " + _currentElementVar;
    }

    public void ResetDefault()
    {
        Time.timeScale = 1;

        Room1Clear = false;
        Room2Clear = false;
        Room3Clear = false;
        Room4Clear = false;
        Room5Clear = false;
        Room6Clear = false;
        Room7Clear = false;

        FristTimeEnterRoom1 = true;
        FristTimeEnterRoom2 = true;
        FristTimeEnterRoom3 = true;
        FristTimeEnterRoom4 = true;
        FristTimeEnterRoom5 = true;
        FristTimeEnterRoom6 = true;
        FristTimeEnterRoom7 = true;

        for (int i = 0; i < GateRoom.Length; i++)
        {
            GateRoom[i].SetActive(false);
        }

        GameRoomManager.Instance.Floor = 1;

        Instantiate(Player, PlayerSpawnPoint.transform.position, Quaternion.identity);
    }

    public void GameOver()
    {
        int Rand = Random.Range(1, 6);
        if (Rand == 1)
        {
            GameoverHeaderText.text = "So Ur HP <= 0";
        }
        if (Rand == 2)
        {
            GameoverHeaderText.text = "HeHeHeHa You lose";
        }
        if (Rand == 3)
        {
            GameoverHeaderText.text = "Try Again or go Home";
        }
        if (Rand == 4)
        {
            GameoverHeaderText.text = "Dead!";
        }
        if (Rand == 5)
        {
            GameoverHeaderText.text = "OwO";
        }

        GameoverPanel.SetActive(true);
        TotalFloortext.text = "Total Floor: " + Floor;
        CallTimeUpdate();
        Time.timeScale = 0;
    }

    public void CampaignClear()
    {
        ClearPanel.SetActive(true);
        TotalFloortext_ClearPanel.text = "Total Floor: " + Floor;
        CallTimeUpdate();
        Time.timeScale = 0;
    }

    public void GameOverRestart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void GameOverGoToMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(sceneBuildIndex: 0);
    }

    public void GameOverExit()
    {
        Application.Quit();
    }

    public void CallPausePanel()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        CallTimeUpdate();
    }

    public void CallTimeUpdate()
    {
        for (int i = 0; i < timerText.Length; i++)
        {
            timerText[i].text = string.Format("Time: {0:00}:{1:00}", minutes, seconds);
        }
    }

    public void HideMarker()
    {
        for (int i = 0; i < marker.Length; i++)
        {
            marker[i].SetActive(false);
        }
    }

    public void IncreaseEnemy()
    {
        NextSpawn += SpawnInterval;
        if (NextSpawn >= 1)
        {
            EnemySpawn += Mathf.CeilToInt(NextSpawn);
            NextSpawn = 0;
        }
    }
}
