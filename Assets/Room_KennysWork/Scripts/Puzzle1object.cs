using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle1object : MonoBehaviour,Damageable
{
    private float HealthTest = 1;
    private _PuzzelTest setNum;
    private string PlayerElement;
    public string BlockElement;
    private ParticleSystem _particleSystem;
    private bool finished;
    private void Awake()
    {
        setNum = GetComponentInParent<_PuzzelTest>();
        _particleSystem = GetComponent<ParticleSystem>();
        finished = false;
    }

    public void DoDamage(float damage)
    {
        PlayerElement = GameRoomManager.Instance._currentElementVar;
        if (BlockElement == PlayerElement)
        {
            HealthTest -= damage;
        }
        if (HealthTest <= 0 && !finished)
        {
            StartCoroutine(inten());
            setNum.blockLeft--;
        }
    }
    IEnumerator inten()
    {
        _particleSystem.Play();
        finished = true;
        yield return new WaitForSeconds(0.2f);
        Destroy(gameObject);
    }
}
