using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using Unity.VisualScripting;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    void LateUpdate()
    {
        GameObject _player = GameObject.Find("PlayerManage(Clone)");

        if (_player != null)
        {
            Vector3 newPos = _player.transform.position;
            newPos.y = transform.position.y;
            transform.position = newPos;

            //transform.rotation = quaternion.Euler(90f, _player.transform.eulerAngles.y, 0f);
        }
    }
}
