using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using TMPro;
using Unity.VisualScripting;
using Debug = System.Diagnostics.Debug;
using Random = UnityEngine.Random;

public class UpgradeShop : MonoBehaviour
{
    public Upgradestat[] UpgradePerset;
    private int Card1RanNUm;
    private int Card2RanNUm;
    private int Card3RanNUm;
    public GameObject upWindow;

    private Upgradestat Card1;
    private Upgradestat Card3;
    private Upgradestat Card2;
    private PlayerManager _playerM;
    [Header("Card1")]
    public TextMeshProUGUI textDesc1;
    public TextMeshProUGUI textStat1;
    public TextMeshProUGUI textAmouth1;
    [Header("Card2")]
    public TextMeshProUGUI textDesc2;
    public TextMeshProUGUI textStat2;
    public TextMeshProUGUI textAmouth2;
    [Header("Card3")]
    public TextMeshProUGUI textDesc3;
    public TextMeshProUGUI textStat3;
    public TextMeshProUGUI textAmouth3;
    
    private string displayElement1;
    Color displayElementColor1;
    private int amount1;
    
    private string displayElement2;
    Color displayElementColor2;
    private int amount2;
    
    private string displayElement3;
    Color displayElementColor3;
    private int amount3;

    public GameObject interF;
    
    public TextMeshProUGUI moneyText;
    public RoomStat moneyAmount;
    private bool isShop;
    
    void Update()
    {
        Card1 = UpgradePerset[Card1RanNUm];
        Card2 = UpgradePerset[Card2RanNUm];
        Card3 = UpgradePerset[Card3RanNUm];
        
        moneyText.SetText("Money"+" : " + moneyAmount.money);
        textAmouth1.SetText(amount1.ToString());
        textAmouth2.SetText(amount2.ToString());
        textAmouth3.SetText(amount3.ToString());
        
        if (Card1.Elementnumber == 0) { displayElement1 = "None"; displayElementColor1 = Color.white;}
        if (Card1.Elementnumber == 1) { displayElement1 = "Water"; displayElementColor1 = Color.cyan;}
        if (Card1.Elementnumber == 2) { displayElement1 = "Earth"; displayElementColor1 = Color.HSVToRGB(.1f,1f,.8f);}
        if (Card1.Elementnumber == 3) { displayElement1 = "Wind"; displayElementColor1 = Color.green;}
        
        if (Card2.Elementnumber == 0) { displayElement2 = "None"; displayElementColor2 = Color.white;}
        if (Card2.Elementnumber == 1) { displayElement2 = "Water"; displayElementColor2 = Color.cyan;}
        if (Card2.Elementnumber == 2) { displayElement2 = "Earth"; displayElementColor2 = Color.HSVToRGB(.1f,1f,.8f);}
        if (Card2.Elementnumber == 3) { displayElement2 = "Wind"; displayElementColor2 = Color.green;}
        
        if (Card3.Elementnumber == 0) { displayElement3 = "None"; displayElementColor3 = Color.white;}
        if (Card3.Elementnumber == 1) { displayElement3 = "Water"; displayElementColor3 = Color.cyan;}
        if (Card3.Elementnumber == 2) { displayElement3 = "Earth"; displayElementColor3 = Color.HSVToRGB(.1f,1f,.8f);}
        if (Card3.Elementnumber == 3) { displayElement3 = "Wind"; displayElementColor3 = Color.green;}
        
        textDesc1.text = Card1.attribute1 + ", " + Card1.attribute2 + ", " + Card1.attribute3 +" " +"UP";
        textStat1.text = displayElement1;
        textStat1.color = displayElementColor1;
        
        textDesc2.text = Card2.attribute1 + ", " + Card2.attribute2 + ", " + Card2.attribute3 +" " +"UP";
        textStat2.text = displayElement2;
        textStat2.color = displayElementColor2;
        
        textDesc3.text = Card3.attribute1 + ", " + Card3.attribute2 + " " + Card3.attribute3 +" " +"UP";
        textStat3.text = displayElement3;
        textStat3.color = displayElementColor3;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            FirstRoll();
            interF.gameObject.SetActive(true);
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerM = other.GetComponent<PlayerManager>();
            if (Input.GetKeyUp(KeyCode.F) && moneyAmount.money >= 10 && !isShop)
            {
                isShop = true;
                FirstRoll();
                upWindow.SetActive(true);
                Time.timeScale = 0;
                moneyAmount.money -= 10;
            }
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            interF.gameObject.SetActive(false);
        }
    }

    public void pickCard1()
    {
        switch(Card1.Elementnumber)
        {
            case 0 : //NoneElementCard
                if(Card1.attribute1 == "ATK"){ AddATK(0,amount1);}
                if(Card1.attribute1 == "HP"){ AddHP(0,amount1);}
                if(Card1.attribute1 == "MANA"){ AddMANA(0,amount1);}
                if(Card1.attribute1 == "SPEED"){ AddSPEED(0,amount1);}
                if(Card1.attribute1 == "FireSpeed"){ AddFIRERATE(0,amount1);}
                    
                if(Card1.attribute2 == "ATK"){ AddATK(0,amount1);}
                if(Card1.attribute2 == "HP"){ AddHP(0,amount1);}
                if(Card1.attribute2 == "MANA"){ AddMANA(0,amount1);}
                if(Card1.attribute2 == "SPEED"){ AddSPEED(0,amount1);}
                if(Card1.attribute2 == "FireSpeed"){ AddFIRERATE(0,amount1);}
                    
                if(Card1.attribute3 == "ATK"){ AddATK(0,amount1);}
                if(Card1.attribute3 == "HP"){ AddHP(0,amount1);}
                if(Card1.attribute3 == "MANA"){ AddMANA(0,amount1);}
                if(Card1.attribute3 == "SPEED"){ AddSPEED(0,amount1);}
                if(Card1.attribute3 == "FireSpeed"){ AddFIRERATE(0,amount1);}
                break;
            case 1 : //WaterElementCard
                if(Card1.attribute1 == "ATK"){ AddATK(1,amount1);}
                if(Card1.attribute1 == "HP"){ AddHP(1,amount1);}
                if(Card1.attribute1 == "MANA"){ AddMANA(1,amount1);}
                if(Card1.attribute1 == "SPEED"){ AddSPEED(1,amount1);}
                if(Card1.attribute1 == "FireSpeed"){ AddFIRERATE(1,amount1);}
                    
                if(Card1.attribute2 == "ATK"){ AddATK(1,amount1);}
                if(Card1.attribute2 == "HP"){ AddHP(1,amount1);}
                if(Card1.attribute2 == "MANA"){ AddMANA(1,amount1);}
                if(Card1.attribute2 == "SPEED"){ AddSPEED(1,amount1);}
                if(Card1.attribute2 == "FireSpeed"){ AddFIRERATE(1,amount1);}
                    
                if(Card1.attribute3 == "ATK"){ AddATK(1,amount1);}
                if(Card1.attribute3 == "HP"){ AddHP(1,amount1);}
                if(Card1.attribute3 == "MANA"){ AddMANA(1,amount1);}
                if(Card1.attribute3 == "SPEED"){ AddSPEED(1,amount1);}
                if(Card1.attribute3 == "FireSpeed"){ AddFIRERATE(1,amount1);}
                break;
            case 2 : //EarthElementCard
                if(Card1.attribute1 == "ATK"){ AddATK(2,amount1);}
                if(Card1.attribute1 == "HP"){ AddHP(2,amount1);}
                if(Card1.attribute1 == "MANA"){ AddMANA(2,amount1);}
                if(Card1.attribute1 == "SPEED"){ AddSPEED(2,amount1);}
                if(Card1.attribute1 == "FireSpeed"){ AddFIRERATE(2,amount1);}
                    
                if(Card1.attribute2 == "ATK"){ AddATK(2,amount1);}
                if(Card1.attribute2 == "HP"){ AddHP(2,amount1);}
                if(Card1.attribute2 == "MANA"){ AddMANA(2,amount1);}
                if(Card1.attribute2 == "SPEED"){ AddSPEED(2,amount1);}
                if(Card1.attribute2 == "FireSpeed"){ AddFIRERATE(2,amount1);}
                    
                if(Card1.attribute3 == "ATK"){ AddATK(2,amount1);}
                if(Card1.attribute3 == "HP"){ AddHP(2,amount1);}
                if(Card1.attribute3 == "MANA"){ AddMANA(2,amount1);}
                if(Card1.attribute3 == "SPEED"){ AddSPEED(2,amount1);}
                if(Card1.attribute3 == "FireSpeed"){ AddFIRERATE(2,amount1);}
                break;
            case 3 : //WindElementCard
                if(Card1.attribute1 == "ATK"){ AddATK(3,amount1);}
                if(Card1.attribute1 == "HP"){ AddHP(3,amount1);}
                if(Card1.attribute1 == "MANA"){ AddMANA(3,amount1);}
                if(Card1.attribute1 == "SPEED"){ AddSPEED(3,amount1);}
                if(Card1.attribute1 == "FireSpeed"){ AddFIRERATE(3,amount1);}
                    
                if(Card1.attribute2 == "ATK"){ AddATK(3,amount1);}
                if(Card1.attribute2 == "HP"){ AddHP(3,amount1);}
                if(Card1.attribute2 == "MANA"){ AddMANA(3,amount1);}
                if(Card1.attribute2 == "SPEED"){ AddSPEED(3,amount1);}
                if(Card1.attribute2 == "FireSpeed"){ AddFIRERATE(3,amount1);}
                    
                if(Card1.attribute3 == "ATK"){ AddATK(3,amount1);}
                if(Card1.attribute3 == "HP"){ AddHP(3,amount1);}
                if(Card1.attribute3 == "MANA"){ AddMANA(3,amount1);}
                if(Card1.attribute3 == "SPEED"){ AddSPEED(3,amount1);}
                if(Card1.attribute3 == "FireSpeed"){ AddFIRERATE(3,amount1);}
                break;
        }
        isShop = false;
        upWindow.SetActive(false);
        Time.timeScale = 1;
    }
    public void pickCard2()
    {
        switch(Card2.Elementnumber)
        {
            case 0 : //NoneElementCard
                if(Card2.attribute1 == "ATK"){ AddATK(0,amount2);}
                if(Card2.attribute1 == "HP"){ AddHP(0,amount2);}
                if(Card2.attribute1 == "MANA"){ AddMANA(0,amount2);}
                if(Card2.attribute1 == "SPEED"){ AddSPEED(0,amount2);}
                if(Card2.attribute1 == "FireSpeed"){ AddFIRERATE(0,amount2);}
                    
                if(Card2.attribute2 == "ATK"){ AddATK(0,amount2);}
                if(Card2.attribute2 == "HP"){ AddHP(0,amount2);}
                if(Card2.attribute2 == "MANA"){ AddMANA(0,amount2);}
                if(Card2.attribute2 == "SPEED"){ AddSPEED(0,amount2);}
                if(Card2.attribute2 == "FireSpeed"){ AddFIRERATE(0,amount2);}
                    
                if(Card2.attribute3 == "ATK"){ AddATK(0,amount2);}
                if(Card2.attribute3 == "HP"){ AddHP(0,amount2);}
                if(Card2.attribute3 == "MANA"){ AddMANA(0,amount2);}
                if(Card2.attribute3 == "SPEED"){ AddSPEED(0,amount2);}
                if(Card2.attribute3 == "FireSpeed"){ AddFIRERATE(0,amount2);}
                break;
            case 1 : //WaterElementCard
                if(Card2.attribute1 == "ATK"){ AddATK(1,amount2);}
                if(Card2.attribute1 == "HP"){ AddHP(1,amount2);}
                if(Card2.attribute1 == "MANA"){ AddMANA(1,amount2);}
                if(Card2.attribute1 == "SPEED"){ AddSPEED(1,amount2);}
                if(Card2.attribute1 == "FireSpeed"){ AddFIRERATE(1,amount2);}
                    
                if(Card2.attribute2 == "ATK"){ AddATK(1,amount2);}
                if(Card2.attribute2 == "HP"){ AddHP(1,amount2);}
                if(Card2.attribute2 == "MANA"){ AddMANA(1,amount2);}
                if(Card2.attribute2 == "SPEED"){ AddSPEED(1,amount2);}
                if(Card2.attribute2 == "FireSpeed"){ AddFIRERATE(1,amount2);}
                    
                if(Card2.attribute3 == "ATK"){ AddATK(1,amount2);}
                if(Card2.attribute3 == "HP"){ AddHP(1,amount2);}
                if(Card2.attribute3 == "MANA"){ AddMANA(1,amount2);}
                if(Card2.attribute3 == "SPEED"){ AddSPEED(1,amount2);}
                if(Card2.attribute3 == "FireSpeed"){ AddFIRERATE(1,amount2);}
                break;
            case 2 : //EarthElementCard
                if(Card2.attribute1 == "ATK"){ AddATK(2,amount2);}
                if(Card2.attribute1 == "HP"){ AddHP(2,amount2);}
                if(Card2.attribute1 == "MANA"){ AddMANA(2,amount2);}
                if(Card2.attribute1 == "SPEED"){ AddSPEED(2,amount2);}
                if(Card2.attribute1 == "FireSpeed"){ AddFIRERATE(2,amount2);}
                    
                if(Card2.attribute2 == "ATK"){ AddATK(2,amount2);}
                if(Card2.attribute2 == "HP"){ AddHP(2,amount2);}
                if(Card2.attribute2 == "MANA"){ AddMANA(2,amount2);}
                if(Card2.attribute2 == "SPEED"){ AddSPEED(2,amount2);}
                if(Card2.attribute2 == "FireSpeed"){ AddFIRERATE(2,amount2);}
                    
                if(Card2.attribute3 == "ATK"){ AddATK(2,amount2);}
                if(Card2.attribute3 == "HP"){ AddHP(2,amount2);}
                if(Card2.attribute3 == "MANA"){ AddMANA(2,amount2);}
                if(Card2.attribute3 == "SPEED"){ AddSPEED(2,amount2);}
                if(Card2.attribute3 == "FireSpeed"){ AddFIRERATE(2,amount2);}
                break;
            case 3 : //WindElementCard
                if(Card2.attribute1 == "ATK"){ AddATK(3,amount2);}
                if(Card2.attribute1 == "HP"){ AddHP(3,amount2);}
                if(Card2.attribute1 == "MANA"){ AddMANA(3,amount2);}
                if(Card2.attribute1 == "SPEED"){ AddSPEED(3,amount2);}
                if(Card2.attribute1 == "FireSpeed"){ AddFIRERATE(3,amount2);}
                    
                if(Card2.attribute2 == "ATK"){ AddATK(3,amount2);}
                if(Card2.attribute2 == "HP"){ AddHP(3,amount2);}
                if(Card2.attribute2 == "MANA"){ AddMANA(3,amount2);}
                if(Card2.attribute2 == "SPEED"){ AddSPEED(3,amount2);}
                if(Card2.attribute2 == "FireSpeed"){ AddFIRERATE(3,amount2);}
                    
                if(Card2.attribute3 == "ATK"){ AddATK(3,amount2);}
                if(Card2.attribute3 == "HP"){ AddHP(3,amount2);}
                if(Card2.attribute3 == "MANA"){ AddMANA(3,amount2);}
                if(Card2.attribute3 == "SPEED"){ AddSPEED(3,amount2);}
                if(Card2.attribute3 == "FireSpeed"){ AddFIRERATE(3,amount2);}
                break;
        }
        isShop = false;
        upWindow.SetActive(false);
        Time.timeScale = 1;
    }
    public void pickCard3()
    {
        switch(Card3.Elementnumber)
        {
            case 0 : //NoneElementCard
                if(Card3.attribute1 == "ATK"){ AddATK(0,amount3);}
                if(Card3.attribute1 == "HP"){ AddHP(0,amount3);}
                if(Card3.attribute1 == "MANA"){ AddMANA(0,amount3);}
                if(Card3.attribute1 == "SPEED"){ AddSPEED(0,amount3);}
                if(Card3.attribute1 == "FireSpeed"){ AddFIRERATE(0,amount3);}
                    
                if(Card3.attribute2 == "ATK"){ AddATK(0,amount3);}
                if(Card3.attribute2 == "HP"){ AddHP(0,amount3);}
                if(Card3.attribute2 == "MANA"){ AddMANA(0,amount3);}
                if(Card3.attribute2 == "SPEED"){ AddSPEED(0,amount3);}
                if(Card3.attribute2 == "FireSpeed"){ AddFIRERATE(0,amount3);}
                    
                if(Card3.attribute3 == "ATK"){ AddATK(0,amount3);}
                if(Card3.attribute3 == "HP"){ AddHP(0,amount3);}
                if(Card3.attribute3 == "MANA"){ AddMANA(0,amount3);}
                if(Card3.attribute3 == "SPEED"){ AddSPEED(0,amount3);}
                if(Card3.attribute3 == "FireSpeed"){ AddFIRERATE(0,amount3);}
                break;
            case 1 : //WaterElementCard
                if(Card3.attribute1 == "ATK"){ AddATK(1,amount3);}
                if(Card3.attribute1 == "HP"){ AddHP(1,amount3);}
                if(Card3.attribute1 == "MANA"){ AddMANA(1,amount3);}
                if(Card3.attribute1 == "SPEED"){ AddSPEED(1,amount3);}
                if(Card3.attribute1 == "FireSpeed"){ AddFIRERATE(1,amount3);}
                    
                if(Card3.attribute2 == "ATK"){ AddATK(1,amount3);}
                if(Card3.attribute2 == "HP"){ AddHP(1,amount3);}
                if(Card3.attribute2 == "MANA"){ AddMANA(1,amount3);}
                if(Card3.attribute2 == "SPEED"){ AddSPEED(1,amount3);}
                if(Card3.attribute2 == "FireSpeed"){ AddFIRERATE(1,amount3);}
                    
                if(Card3.attribute3 == "ATK"){ AddATK(1,amount3);}
                if(Card3.attribute3 == "HP"){ AddHP(1,amount3);}
                if(Card3.attribute3 == "MANA"){ AddMANA(1,amount3);}
                if(Card3.attribute3 == "SPEED"){ AddSPEED(1,amount3);}
                if(Card3.attribute3 == "FireSpeed"){ AddFIRERATE(1,amount3);}
                break;
            case 2 : //EarthElementCard
                if(Card3.attribute1 == "ATK"){ AddATK(2,amount3);}
                if(Card3.attribute1 == "HP"){ AddHP(2,amount3);}
                if(Card3.attribute1 == "MANA"){ AddMANA(2,amount3);}
                if(Card3.attribute1 == "SPEED"){ AddSPEED(2,amount3);}
                if(Card3.attribute1 == "FireSpeed"){ AddFIRERATE(2,amount3);}
                    
                if(Card3.attribute2 == "ATK"){ AddATK(2,amount3);}
                if(Card3.attribute2 == "HP"){ AddHP(2,amount3);}
                if(Card3.attribute2 == "MANA"){ AddMANA(2,amount3);}
                if(Card3.attribute2 == "SPEED"){ AddSPEED(2,amount3);}
                if(Card3.attribute2 == "FireSpeed"){ AddFIRERATE(2,amount3);}
                    
                if(Card3.attribute3 == "ATK"){ AddATK(2,amount3);}
                if(Card3.attribute3 == "HP"){ AddHP(2,amount3);}
                if(Card3.attribute3 == "MANA"){ AddMANA(2,amount3);}
                if(Card3.attribute3 == "SPEED"){ AddSPEED(2,amount3);}
                if(Card3.attribute3 == "FireSpeed"){ AddFIRERATE(2,amount3);}
                break;
            case 3 : //WindElementCard
                if(Card3.attribute1 == "ATK"){ AddATK(3,amount3);}
                if(Card3.attribute1 == "HP"){ AddHP(3,amount3);}
                if(Card3.attribute1 == "MANA"){ AddMANA(3,amount3);}
                if(Card3.attribute1 == "SPEED"){ AddSPEED(3,amount3);}
                if(Card3.attribute1 == "FireSpeed"){ AddFIRERATE(3,amount3);}
                    
                if(Card3.attribute2 == "ATK"){ AddATK(3,amount3);}
                if(Card3.attribute2 == "HP"){ AddHP(3,amount3);}
                if(Card3.attribute2 == "MANA"){ AddMANA(3,amount3);}
                if(Card3.attribute2 == "SPEED"){ AddSPEED(3,amount3);}
                if(Card3.attribute2 == "FireSpeed"){ AddFIRERATE(3,amount3);}
                    
                if(Card3.attribute3 == "ATK"){ AddATK(3,amount3);}
                if(Card3.attribute3 == "HP"){ AddHP(3,amount3);}
                if(Card3.attribute3 == "MANA"){ AddMANA(3,amount3);}
                if(Card3.attribute3 == "SPEED"){ AddSPEED(3,amount3);}
                if(Card3.attribute3 == "FireSpeed"){ AddFIRERATE(3,amount3);}
                break;
        }
        isShop = false;
        upWindow.SetActive(false);
        Time.timeScale = 1;
    }

    void AddATK(int num,int cardAmount)
    {
        _playerM.data[num].Damage += cardAmount;
    }
    void AddHP(int num,int cardAmount)
    {
        _playerM.data[num].MaxHp += cardAmount * 10;
    }
    void AddMANA(int num,int cardAmount)
    {
        _playerM.data[num].MaxMana += cardAmount * 10;
    }
    void AddSPEED(int num,int cardAmount)
    {
        _playerM.data[num].Speed += Mathf.Clamp(cardAmount, 1, 3);
    }
    void AddFIRERATE(int num,int cardAmount)
    {
        _playerM.data[num].FireSpeed -= cardAmount * .05f;
    }

    public void ReRoll()
    {
        if (moneyAmount.money >= 5)
        {
            moneyAmount.money -= 5;
            Card1RanNUm = Random.Range(0, UpgradePerset.Length + 1);
            Card2RanNUm = Random.Range(0, UpgradePerset.Length + 1);
            Card3RanNUm = Random.Range(0, UpgradePerset.Length + 1);
            amount1 = Random.Range(1, 6);
            amount2 = Random.Range(1, 6);
            amount3 = Random.Range(1, 6);
        }
    }

    private void FirstRoll()
    {
        Card1RanNUm = Random.Range(0, UpgradePerset.Length + 1);
        Card2RanNUm = Random.Range(0, UpgradePerset.Length + 1);
        Card3RanNUm = Random.Range(0, UpgradePerset.Length + 1);
        amount1 = Random.Range(1, 6);
        amount2 = Random.Range(1, 6);
        amount3 = Random.Range(1, 6);
    }
}
