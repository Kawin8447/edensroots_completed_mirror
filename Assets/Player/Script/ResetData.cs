using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetData : MonoBehaviour
{
    private PlayerManager _playerManager;
    public ObjectStat[] defaultdata;
    public RoomStat roomData;

    private void Awake()
    {
        _playerManager = gameObject.GetComponent<PlayerManager>();
    }

    public void ResetStatData()
    {
        Debug.Log("ResetData");
        roomData.money = 0;
        _playerManager.data[0].MaxHp = defaultdata[0].MaxHp;
        _playerManager.data[0].Damage = defaultdata[0].Damage;
        _playerManager.data[0].MaxMana = defaultdata[0].MaxMana;
        _playerManager.data[0].Speed = defaultdata[0].Speed;
        _playerManager.data[0].FireSpeed = defaultdata[0].FireSpeed;
        _playerManager.data[0].Energy = defaultdata[0].Energy;
        _playerManager.data[0].SkillCooldown = defaultdata[0].SkillCooldown;
        
        _playerManager.data[1].MaxHp = defaultdata[1].MaxHp;
        _playerManager.data[1].Damage = defaultdata[1].Damage;
        _playerManager.data[1].MaxMana = defaultdata[1].MaxMana;
        _playerManager.data[1].Speed = defaultdata[1].Speed;
        _playerManager.data[1].FireSpeed = defaultdata[1].FireSpeed;
        _playerManager.data[1].Energy = defaultdata[1].Energy;
        _playerManager.data[1].SkillCooldown = defaultdata[1].SkillCooldown;
        
        _playerManager.data[2].MaxHp = defaultdata[2].MaxHp;
        _playerManager.data[2].Damage = defaultdata[2].Damage;
        _playerManager.data[2].MaxMana = defaultdata[2].MaxMana;
        _playerManager.data[2].Speed = defaultdata[2].Speed;
        _playerManager.data[2].FireSpeed = defaultdata[2].FireSpeed;
        _playerManager.data[2].Energy = defaultdata[2].Energy;
        _playerManager.data[2].SkillCooldown = defaultdata[2].SkillCooldown;
        
        _playerManager.data[3].MaxHp = defaultdata[3].MaxHp;
        _playerManager.data[3].Damage = defaultdata[3].Damage;
        _playerManager.data[3].MaxMana = defaultdata[3].MaxMana;
        _playerManager.data[3].Speed = defaultdata[3].Speed;
        _playerManager.data[3].FireSpeed = defaultdata[3].FireSpeed;
        _playerManager.data[3].Energy = defaultdata[3].Energy;
        _playerManager.data[3].SkillCooldown = defaultdata[3].SkillCooldown;
    }
}
