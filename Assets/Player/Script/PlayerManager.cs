using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using TMPro;
using Unity.VisualScripting;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public enum Status
{
    Idle,
    Move,
    Stunt
};

public enum CurrentElement
{
    None,
    Earth,
    Water,
    Wind
};

public class PlayerManager : MonoBehaviour, Damageable
{
    [Header("PlayerStatus")]
    public Status _Status;
    public CurrentElement _Element;
    public float invincibleLength;
    [SerializeField] private GameObject player;
    public Vector3 currentPosition;

    [Header("PlayerData")]
    public ObjectStat[] data;

    [Header("Ult Object")]
    public GameObject[] ult;

    [Header("Skill Object")]
    public GameObject[] Skill;

    [Header("Stat")]
    public float playerHp;
    public float maxPlayerHp;
    public float playerMana;
    public float maxPlayerMana;
    public int damage;
    public int speed;
    public float fireRate;
    public float curUltEnergy;
    public float MaxultEnergy;
    public float cooldownTime;
    public int DashRange;
    public int DashCooldown;

    [Header("Material")]
    [SerializeField] private GameObject[] tranformEffect;
    public static bool gameIsPaused;

    //Value
    public float valueLoss = 0.5f;
    public float invincibillityCounter;
    public float cooldownDuration;
    public bool isCooldown;

    [Header("Damage Indecator")]
    public Animator DamageAnim;
    public TextMeshProUGUI DamageText;
    private float TotalDamage;

    private void Awake()
    {
        DamageText.gameObject.SetActive(false);
        DamageText.color = Color.clear;
    }

    private void Start()
    {
        //Reset Func
        ResetData re = gameObject.GetComponent<ResetData>();
        re.ResetStatData();
    }
    void Update()
    {
        tranformEffect[0].transform.position = player.transform.position;
        tranformEffect[1].transform.position = player.transform.position;
        tranformEffect[2].transform.position = player.transform.position;
        tranformEffect[3].transform.position = player.transform.position;
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CallPause();
        }
        if (player != null)
        {
            currentPosition = player.transform.position;
            transform.position = player.transform.position;
        }
        else
        {
            Debug.Log("Assign Player Prefab");
        }
        if (isCooldown)
        {
            cooldownDuration -= Time.deltaTime;
            if (cooldownDuration <= 0f)
            {
                isCooldown = false;
                Debug.Log("Ability is ready!");
            }
        }

        if (invincibillityCounter > 0)
        {
            invincibillityCounter -= Time.deltaTime;
        }
        if (playerMana <= 0)
        {
            _Element = CurrentElement.None;
            TranformElement("None");
        }

        //DecayHp if overhealth
        if (playerHp > maxPlayerHp)
        {
                playerHp -= valueLoss * Time.deltaTime;
        }
        else
        {
            playerHp = Mathf.Round(playerHp * 10.0f) * 0.1f;
        }

        if (curUltEnergy > MaxultEnergy)
        {
            curUltEnergy = MaxultEnergy;
        }
        else if (curUltEnergy < 0)
        {
            curUltEnergy = 0;
        }
        switch (_Status)
        {
            case Status.Idle:
                break;
            case Status.Move:
                break;
            case Status.Stunt:
                break;
        }
        switch (_Element)
        {
            case CurrentElement.None:
                maxPlayerHp = data[0].MaxHp;
                maxPlayerMana = data[0].MaxMana;
                damage = data[0].Damage;
                speed = data[0].Speed;
                fireRate = data[0].FireSpeed;
                MaxultEnergy = data[0].Energy;
                cooldownTime = data[0].SkillCooldown;
                playerMana = maxPlayerMana; //inf mana
                GameRoomManager.Instance._currentElementVar = "None";
                break;
            case CurrentElement.Water:
                maxPlayerHp = data[1].MaxHp;
                maxPlayerMana = data[1].MaxMana;
                damage = data[1].Damage;
                speed = data[1].Speed;
                fireRate = data[1].FireSpeed;
                MaxultEnergy = data[1].Energy;
                cooldownTime = data[1].SkillCooldown;
                GameRoomManager.Instance._currentElementVar = "Water";
                break;
            case CurrentElement.Earth:
                maxPlayerHp = data[2].MaxHp;
                maxPlayerMana = data[2].MaxMana;
                damage = data[2].Damage;
                speed = data[2].Speed;
                fireRate = data[2].FireSpeed;
                MaxultEnergy = data[2].Energy;
                cooldownTime = data[2].SkillCooldown;
                GameRoomManager.Instance._currentElementVar = "Earth";
                break;
            case CurrentElement.Wind:
                maxPlayerHp = data[3].MaxHp;
                maxPlayerMana = data[3].MaxMana;
                damage = data[3].Damage;
                speed = data[3].Speed;
                fireRate = data[3].FireSpeed;
                MaxultEnergy = data[3].Energy;
                cooldownTime = data[3].SkillCooldown;
                GameRoomManager.Instance._currentElementVar = "Wind";
                break;
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            DoDamage(100);
            playerMana -= 10;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            HealPlayer(100);
            playerMana += 10;
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            Debug.LogWarning("God Mode On");
            debugModeOn();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            Debug.LogWarning("God Mode off");
            debugModeOff();
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _Element = CurrentElement.None;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _Element = CurrentElement.Earth;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            _Element = CurrentElement.Water;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            _Element = CurrentElement.Wind;
        }
    }

    public void CallPause()
    {
        gameIsPaused = !gameIsPaused;
        PauseGame();
        GameRoomManager.Instance.CallPausePanel();
    }
    void PauseGame()
    {
        if (gameIsPaused)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1;
        }
    }
    /*private void OnTriggerEnter(Collider otherCol)
    {
        if (otherCol.gameObject.name == "enemyBullet" && playerHp > 0)
        {
            StartCoroutine("Invincible");
        }
    }*/
    public void ActivateAbility()
    {
        if (!isCooldown)
        {
            isCooldown = true;
            cooldownDuration = cooldownTime;
            Debug.Log("Ability activated!");
        }
        else
        {
            Debug.Log("Ability is on cooldown!");
        }
    }
    public void DoDamage(float damage)
    {
        if (playerHp > 0 && invincibillityCounter <= 0)
        {
            HitPlayer(damage, Vector3.zero);
            StartCoroutine("Invincible");

            if (DamageAnim.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.33f)
            {
                TotalDamage += damage;
                DamageText.text = TotalDamage.ToString();
                DamageText.color = Color.red;
                DamageAnim.SetTrigger("GetAnDamage");

            }
            else
            {
                TotalDamage = damage;
                DamageText.text = TotalDamage.ToString();
                DamageText.color = Color.red;
                DamageAnim.SetTrigger("GetAnDamage");
            }
        }
        else if (invincibillityCounter >= 0)
        {
            DamageText.text = "Immune";
            DamageText.color = Color.gray;
            DamageAnim.SetTrigger("GetAnDamage");
        }
    }

    public IEnumerable Invincible()
    {
        player.GetComponent<Renderer>();
        Physics.IgnoreLayerCollision(7, 7, true);
        yield return new WaitForSeconds(invincibleLength);
        Physics.IgnoreLayerCollision(7, 7, false);
    }

    public void HitPlayer(float damage, Vector3 dir)
    {
        if (invincibillityCounter <= 0)
        {
            if (DamageText.gameObject.activeSelf == false)
            {
                DamageText.gameObject.SetActive(true);
            }

            playerHp -= damage;
            invincibillityCounter = invincibleLength;

            if (playerHp <= 0)
            {
                GameRoomManager.Instance.GameOver();
            }
        }
    }

    public void HealPlayer(int heal)
    {
        playerHp += heal;

        if (DamageText.gameObject.activeSelf == false)
        {
            DamageText.gameObject.SetActive(true);
        }

        DamageText.text = heal.ToString();
        DamageText.color = Color.green;
        DamageAnim.SetTrigger("GetAnDamage");
    }

    public void TranformElement(string Element)
    {
        if (Element == "None")
        {
            Instantiate(tranformEffect[0], player.transform.position, tranformEffect[0].transform.rotation);
        }
        else if (Element == "Water")
        {
            Instantiate(tranformEffect[1], player.transform.position, tranformEffect[1].transform.rotation);
        }
        else if (Element == "Earth")
        {
            Instantiate(tranformEffect[2], player.transform.position, tranformEffect[2].transform.rotation);
        }
        else if (Element == "Wind")
        {
            Instantiate(tranformEffect[3], player.transform.position, Quaternion.identity);
        }
    }

    public void debugModeOn()
    {
        data[0].Damage *= 10;
        data[1].Damage *= 10;
        data[2].Damage *= 10;
        data[3].Damage *= 10;

        data[0].Speed *= 5;
        data[1].Speed *= 5;
        data[2].Speed *= 5;
        data[3].Speed *= 5;

        data[0].FireSpeed /= 10;
        data[1].FireSpeed /= 10;
        data[2].FireSpeed /= 10;
        data[3].FireSpeed /= 10;

        data[0].MaxHp *= 10;
        data[1].MaxHp *= 10;
        data[2].MaxHp *= 10;
        data[3].MaxHp *= 10;

        playerHp *= 10;
        playerHp *= 10;
        playerHp *= 10;
        playerHp *= 10;
    }
    public void debugModeOff()
    {
        data[0].Damage /= 10;
        data[1].Damage /= 10;
        data[2].Damage /= 10;
        data[3].Damage /= 10;

        data[0].Speed /= 5;
        data[1].Speed /= 5;
        data[2].Speed /= 5;
        data[3].Speed /= 5;

        data[0].FireSpeed *= 10;
        data[1].FireSpeed *= 10;
        data[2].FireSpeed *= 10;
        data[3].FireSpeed *= 10;

        data[0].MaxHp /= 10;
        data[1].MaxHp /= 10;
        data[2].MaxHp /= 10;
        data[3].MaxHp /= 10;

        playerHp /= 10;
        playerHp /= 10;
        playerHp /= 10;
        playerHp /= 10;
    }
}
