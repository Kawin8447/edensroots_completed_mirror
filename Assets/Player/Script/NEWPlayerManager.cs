using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum NEWStatus
{
    Idle,
    Move,
    Stunt
};

public enum NEWCurrentElement
{
    None,
    Earth,
    Water,
    Wind
};

public class NEWPlayerManager : MonoBehaviour
{
    [Header("PlayerStatus")]
    public Status _Status;
    public CurrentElement _Element;
    public bool isInvincible;
    [SerializeField] private GameObject player;
    public Vector3 currentPosition;
    
    [Header("PlayerData")]
    public ObjectStat[] data;
    
    [Header("Stat")]
    public float playerHp;
    public float maxPlayerHp;
    public int playerMana;
    public int maxPlayerMana;
    public float damage;
    public int speed;
    public float fireRate;
    public int curUltEnergy;
    public int MaxultEnergy;
    public float cooldownTime;
    
    [Header("Material")]
    private Renderer pRender;
    public Material[] _material;
    
    //Value
    private float valueLoss = 0.5f;
    private void Start()
    {
        if (player != null)
        {
            pRender = player.GetComponent<Renderer>();
        }
        //Reset Func
        ResetData re = gameObject.GetComponent<ResetData>();
        re.ResetStatData();
        
    }

    void Update()
    {
        if (player != null)
        {
            currentPosition = player.transform.position;
            transform.position = player.transform.position;
        }
        else
        {
            Debug.Log("Assign Player Prefab");
        }

        if (playerMana < 0)
        {
            _Element = CurrentElement.None;
        }
        
        //DecayHp if overhealth
        if (playerHp > maxPlayerHp)
        {
            playerHp -= valueLoss * Time.deltaTime;
        }
        else
        {
            playerHp = Mathf.Round(playerHp * 10.0f) * 0.1f;
        }
        //DecayEnergy if overEnergy
        if (curUltEnergy > MaxultEnergy)
        {
            playerHp -= valueLoss * Time.deltaTime;
        } 
        switch (_Status)
        {
            case Status.Idle:
                break;
            case Status.Move:
                break;
            case Status.Stunt:
                break;
        }
        switch (_Element)
        {
            case CurrentElement.None:
                pRender.material = _material[0];
                maxPlayerHp = data[0].MaxHp;
                maxPlayerMana = data[0].MaxMana;
                damage = data[0].Damage;
                speed = data[0].Speed;
                fireRate = data[0].FireSpeed;
                MaxultEnergy = data[0].Energy;
                cooldownTime = data[0].SkillCooldown;
                playerMana = maxPlayerMana; //inf mana
                break;
            case CurrentElement.Water:
                pRender.material = _material[1];
                maxPlayerHp = data[1].MaxHp;
                maxPlayerMana = data[1].MaxMana;
                damage = data[1].Damage;
                speed = data[1].Speed;
                fireRate = data[1].FireSpeed;
                MaxultEnergy = data[1].Energy;
                cooldownTime = data[1].SkillCooldown;
                break;
            case CurrentElement.Earth:
                pRender.material = _material[2];
                maxPlayerHp = data[2].MaxHp;
                maxPlayerMana = data[2].MaxMana;
                damage = data[2].Damage;
                speed = data[2].Speed;
                fireRate = data[2].FireSpeed;
                MaxultEnergy = data[2].Energy;
                cooldownTime = data[2].SkillCooldown;
                break;
            case CurrentElement.Wind:
                pRender.material = _material[3];
                maxPlayerHp = data[3].MaxHp;
                maxPlayerMana = data[3].MaxMana;
                damage = data[3].Damage;
                speed = data[3].Speed;
                fireRate = data[3].FireSpeed;
                MaxultEnergy = data[3].Energy;
                cooldownTime = data[3].SkillCooldown;
                break;
        }
    }

    private void OnTriggerEnter(Collider otherCol)
    {
        if (otherCol.gameObject.name == "enemyBullet" && playerHp > 0)
        {
            StartCoroutine("Invincible");
        }
    }

    IEnumerable Invincible()
    {
        player.GetComponent<Renderer>();
        Physics.IgnoreLayerCollision(9,10,true);
        yield return new WaitForSeconds(3f);
        Physics.IgnoreLayerCollision(9,10,false);
    }
   
}
