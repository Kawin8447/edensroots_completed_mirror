using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Upgrade", menuName = "Upgrade")]

public class Upgradestat : ScriptableObject
{
    public int Elementnumber;
    public string attribute1;
    public string attribute2;
    public string attribute3;
    

}

