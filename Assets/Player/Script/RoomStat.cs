using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "RoomStat")]
public class RoomStat : ScriptableObject
{
    public int money;
    public int floor;
    public int puzzleRoomClear;
    public int enemyRoomClear;
    
    [Header("Camp1")]
    public int bestMin1;
    public int bestSec1;
    [Header("Camp2")]
    public int bestMin2;
    public int bestSec2;
    [Header("Camp3")]
    public int bestMin3;
    public int bestSec3;
}
