using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "PlayerStat")]
public class ObjectStat : ScriptableObject
{
    public float MaxHp;
    public int MaxMana;
    public int Damage;
    public int Speed;
    public float FireSpeed;
    public int Energy;
    public float SkillCooldown;
}
