using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControll : MonoBehaviour
{
    public Transform player;
    public float movespeed;
    public Vector3 offset;
    public float followDistance;
    public Quaternion rotation;
    public PlayerControllerSys pControll;
    public GameObject _cameraHolder;

    // Update is called once per frame
    private float teleportDistanceThreshold = 100f;
    private Vector3 newoffset;

    void Update()
    {
        newoffset = new Vector3(pControll._input.x, 2.67f, pControll._input.y);
        //Debug.Log(newoffset);
        _cameraHolder.transform.localPosition = newoffset;
        if (Vector3.Distance(transform.position, player.position) > teleportDistanceThreshold)
        {
            transform.position = player.position + offset + -transform.forward * followDistance;
        }
        else
        {
            Vector3 pos = Vector3.Lerp(transform.position, player.position + offset + -transform.forward * followDistance,
                movespeed * Time.deltaTime);
            transform.position = pos;
        }
        transform.rotation = rotation;
    }
}
