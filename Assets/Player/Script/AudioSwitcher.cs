using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSwitcher : MonoBehaviour
{
    public AudioClip audioClip1; // First audio clip
    public AudioClip audioClip2; // Second audio clip
    public float transitionTime = 1f; // Transition time in seconds
    private AudioSource audioSource;
    private bool isClip1Playing = true; // Flag to track which clip is currently playing

    private void Start()
    {
        // Get the AudioSource component attached to the GameObject
        audioSource = GetComponent<AudioSource>();

        // Play the first audio clip initially
        PlayClip1();
    }
    
    public void SwitchSound()
    {
        if (isClip1Playing)
        {
            StartCoroutine(TransitionClip(audioClip2));
            isClip1Playing = false;
        }
        else
        {
            StartCoroutine(TransitionClip(audioClip1));
            isClip1Playing = true;
        }
    }
    private void PlayClip1()
    {
        if (audioClip1 != null)
        {
            audioSource.clip = audioClip1;
            audioSource.Play();
        }
        else
        {
            Debug.LogWarning("Audio Clip 1 is not assigned!");
        }
    }

    private void PlayClip2()
    {
        if (audioClip2 != null)
        {
            audioSource.clip = audioClip2;
            audioSource.Play();
        }
        else
        {
            Debug.LogWarning("Audio Clip 2 is not assigned!");
        }
    }

    private IEnumerator TransitionClip(AudioClip nextClip)
    {
        float timer = 0f;
        float startVolume = audioSource.volume;

        // Fade out the current clip
        while (timer < transitionTime)
        {
            audioSource.volume = Mathf.Lerp(startVolume, 0f, timer / transitionTime);
            timer += Time.deltaTime;
            yield return null;
        }

        // Switch to the next clip
        audioSource.clip = nextClip;
        audioSource.Play();

        // Fade in the next clip
        timer = 0f;
        while (timer < transitionTime)
        {
            audioSource.volume = Mathf.Lerp(0f, startVolume, timer / transitionTime);
            timer += Time.deltaTime;
            yield return null;
        }

        // Reset volume to the original level
        audioSource.volume = startVolume;
    }
}
