using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class GrowVinesScript : MonoBehaviour
{
    public MeshRenderer meshMaterial; // Reference to the material with the Shader Graph
    public string floatProperty = "Grow_";
    public float Duration = 2f; 
    private float currentValue = 1f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(DecreaseValue());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StartCoroutine(IncreaseValue());
        }
    }

    private void Update()
    {
        if (meshMaterial != null)
        {
            meshMaterial.material.SetFloat(floatProperty, currentValue);
        }
    }
    private IEnumerator IncreaseValue()
    {
        float startTime = Time.time;
        float endTime = startTime + Duration;

        while (Time.time < endTime)
        {
            float progress = (Time.time - startTime) / Duration;
            currentValue = Mathf.Lerp(0f, 1f, progress);
            yield return null;
        }

        currentValue = 1f; 
    }
    private IEnumerator DecreaseValue()
    {
        float startTime = Time.time;
        float endTime = startTime + Duration;

        while (Time.time < endTime)
        {
            float progress = (Time.time - startTime) / Duration;
            currentValue = Mathf.Lerp(1f, 0f, progress);
            yield return null;
        }

        currentValue = 0f;
    }

}
