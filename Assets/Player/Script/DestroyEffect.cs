using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEffect : MonoBehaviour
{
    private GameObject player;
    void Awake()
    { 
        player = GameObject.Find("Player");
       Invoke("EfxDestroy",1f);
    }
    void Update()
    {
        transform.position = new Vector3(player.transform.position.x,2.0f,player.transform.position.z);
    }
    void EfxDestroy()
    {
        Destroy(gameObject);
    }
}
