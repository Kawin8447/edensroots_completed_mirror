using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Remana : MonoBehaviour
{
    private PlayerManager _playerManager;

    private bool CanPingPong = false;
    public bool DontDestroy = false;
    public float lifetime = 15f;

    public GameObject InteractionHint;

    private float HoldingTime;

    private bool ReadyToPickUp = false;

    private void Awake()
    {
        transform.position = new Vector3(transform.position.x, 15, transform.position.z);
    }
    private void Start()
    {
        if (!DontDestroy)
        {
            StartCoroutine(DestroyAfterLifetime());
        }
        InteractionHint.gameObject.SetActive(false);
    }
    private void FixedUpdate()
    {
        if (!CanPingPong)
        {
            transform.Translate(Vector3.down * (Time.deltaTime * 2.25f));
            if (transform.position.y <= 3.75)
            {
                CanPingPong = true;
            }
        }
        if (CanPingPong)
        {
            float newY = Mathf.SmoothStep(2.5f, 4f, Mathf.PingPong(Time.time, 1f));
            transform.position = new Vector3(transform.position.x, newY, transform.position.z);
            gameObject.layer = LayerMask.NameToLayer("Default");
        }

        HoldingTime--;
        if (HoldingTime <= 0)
        {
            InteractionHint.gameObject.SetActive(false);
        }

        if (ReadyToPickUp && !DontDestroy)
        {
            Debug.LogWarning("Am gonna pick you");
            GameObject _player = GameObject.Find("PlayerManage(Clone)");
            float distanceToPlayer = Vector3.Distance(transform.position, _player.transform.position);
            transform.position = Vector3.MoveTowards(transform.position, _player.transform.position, ((distanceToPlayer / 15) + (Time.deltaTime * 5)));

        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (ReadyToPickUp && !DontDestroy)
        {
            _playerManager = other.gameObject.GetComponent<PlayerManager>();
            if (gameObject.name == "ReWater(Clone)" && other.gameObject.tag == "Player")
            {
                _playerManager._Element = CurrentElement.Water;
                _playerManager.TranformElement("Water");
                _playerManager.playerMana = _playerManager.data[1].MaxMana;
                if (!DontDestroy)
                {
                    _playerManager.cooldownDuration = _playerManager.cooldownTime;
                    _playerManager.curUltEnergy -= 10;
                }
                Destroy(this.gameObject);
            }
            if (gameObject.name == "ReEarth(Clone)" && other.gameObject.tag == "Player")
            {
                _playerManager._Element = CurrentElement.Earth;
                _playerManager.TranformElement("Earth");
                _playerManager.playerMana = _playerManager.data[2].MaxMana;
                if (!DontDestroy)
                {
                    _playerManager.cooldownDuration = _playerManager.cooldownTime;
                    _playerManager.curUltEnergy -= 10;
                }
                Destroy(this.gameObject);
            }
            if (gameObject.name == "ReWind(Clone)" && other.gameObject.tag == "Player")
            {
                _playerManager._Element = CurrentElement.Wind;
                _playerManager.TranformElement("Wind");
                _playerManager.playerMana = _playerManager.data[3].MaxMana;
                if (!DontDestroy)
                {
                    _playerManager.cooldownDuration = _playerManager.cooldownTime;
                    _playerManager.curUltEnergy -= 10;
                }
                Destroy(this.gameObject);
            }
        }

        if (DontDestroy)
        {
            _playerManager = other.gameObject.GetComponent<PlayerManager>();
            if (gameObject.name == "ReWater(Clone)" && other.gameObject.tag == "Player" && GameRoomManager.Instance._currentElementVar != "Water")
            {
                _playerManager._Element = CurrentElement.Water;
                _playerManager.TranformElement("Water");
                _playerManager.playerMana = _playerManager.data[1].MaxMana;
            }
            if (gameObject.name == "ReEarth(Clone)" && other.gameObject.tag == "Player" && GameRoomManager.Instance._currentElementVar != "Earth")
            {
                _playerManager._Element = CurrentElement.Earth;
                _playerManager.TranformElement("Earth");
                _playerManager.playerMana = _playerManager.data[2].MaxMana;
            }
            if (gameObject.name == "ReWind(Clone)" && other.gameObject.tag == "Player" && GameRoomManager.Instance._currentElementVar != "Wind")
            {
                _playerManager._Element = CurrentElement.Wind;
                _playerManager.TranformElement("Wind");
                _playerManager.playerMana = _playerManager.data[3].MaxMana;
            }
        }

        if (other.CompareTag("Player"))
        {
            ShowHint();
            if (Input.GetKey(KeyCode.F))
            {
                CallPickup();
            }
        }
    }
    private IEnumerator DestroyAfterLifetime()
    {
        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);
    }

    public void ShowHint()
    {
        if (!DontDestroy)
        {
            HoldingTime = 1;
            InteractionHint.gameObject.SetActive(true);
        }
    }
    public void CallPickup()
    {
        if (!DontDestroy)
        {
            ReadyToPickUp = true;
            Debug.LogWarning("Let me pick you up");
        }
    }
}
