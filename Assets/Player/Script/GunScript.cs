using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{
    [SerializeField] private Transform[] offsetPoint;
    [SerializeField] private GameObject[] ProjectilePrefab;
    [SerializeField] private float firingSpeed;
    [SerializeField] private float angle;
    private PlayerManager _playerManager;

    public static GunScript Instance;
    void Awake()
    {
        Instance = GetComponent<GunScript>();
        _playerManager = GetComponentInParent<PlayerManager>();
    }
    public void Shoot()
    {
        if (_playerManager._Element == CurrentElement.None)
        {
            Instantiate(ProjectilePrefab[0], offsetPoint[0].position, offsetPoint[0].rotation);
        }else if (_playerManager._Element == CurrentElement.Water)
        {
            Instantiate(ProjectilePrefab[1], offsetPoint[0].position, offsetPoint[0].rotation);
        }else if (_playerManager._Element == CurrentElement.Earth)
        {
            Instantiate(ProjectilePrefab[2], offsetPoint[0].position, offsetPoint[0].rotation);
            Instantiate(ProjectilePrefab[2], offsetPoint[1].position, offsetPoint[1].rotation);
            Instantiate(ProjectilePrefab[2], offsetPoint[2].position, offsetPoint[2].rotation);
        }else if (_playerManager._Element == CurrentElement.Wind)
        {
            Instantiate(ProjectilePrefab[3], offsetPoint[0].position, offsetPoint[0].rotation);
        }
        
    }
}
