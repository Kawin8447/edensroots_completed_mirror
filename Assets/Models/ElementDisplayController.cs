using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

public class ElementDisplayController : MonoBehaviour
{

        [SerializeField] private Material[] playerMaterial;
        [SerializeField] private Material[] slimeMaterialNone;
        [SerializeField] private Material[] slimeMaterialEarth;
        [SerializeField] private Material[] slimeMaterialWater;
        [SerializeField] private Material[] slimeMaterialWind;
        

        [SerializeField] private MeshRenderer slime;
        [SerializeField] private SkinnedMeshRenderer body;
        private PlayerManager _playerStat;
        private GameObject _playerManager;
        //private Renderer _slimeRen;
        //private Renderer bodyRenderer;
        private bool IsSet = false;
        
        void Awake()
        {
                Invoke("FindPlayerManage", 0.1f);
        }
        
        void FindPlayerManage()
        {
                _playerManager = GameObject.FindWithTag("Player");
                _playerStat = _playerManager.GetComponent<PlayerManager>();
                //Debug.Log(slime.materials[1].name);
                //false = ready to dash
                IsSet = true;

        }
        private void Update()
        {
                Debug.Log(IsSet);
                if (IsSet == true)
                {
                        switch (_playerStat._Element)
                        { case (CurrentElement.None):
                                        slime.materials = slimeMaterialNone;
                                        body.material = playerMaterial[0];
                                        /*playerMaterial.EnableKeyword("_CURRENT_ELEMENT_NONE");
                                        slimeMaterial.EnableKeyword("_CURRENT_ELEMENT_NONE");
                                        slimeEyesMaterial.EnableKeyword("_CURRENT_ELEMENT_NONE");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_EARTH");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_EARTH");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_EARTH");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_WATER");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_WATER");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_WATER");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_WIND");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_WIND");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_WIND");*/
                                 
                                        break;
                                case (CurrentElement.Earth):
                                        slime.materials = slimeMaterialEarth;
                                        body.material = playerMaterial[1];
                                       /*playerMaterial.EnableKeyword("_CURRENT_ELEMENT_EARTH");
                                        slimeMaterial.EnableKeyword("_CURRENT_ELEMENT_EARTH");
                                        slimeEyesMaterial.EnableKeyword("_CURRENT_ELEMENT_EARTH");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_NONE");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_NONE");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_NONE");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_WATER");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_WATER");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_WATER");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_WIND");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_WIND");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_WIND"); */ 
                                        break;
                                case (CurrentElement.Water):
                                        slime.materials = slimeMaterialWater;
                                        body.material = playerMaterial[2];
                                        /* playerMaterial.EnableKeyword("_CURRENT_ELEMENT_WATER");
                                        slimeMaterial.EnableKeyword("_CURRENT_ELEMENT_WATER");
                                        slimeEyesMaterial.EnableKeyword("_CURRENT_ELEMENT_WATER");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_EARTH");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_EARTH");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_EARTH");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_NONE");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_NONE");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_NONE");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_WIND");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_WIND");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_WIND");*/
                                        break;
                                case (CurrentElement.Wind):
                                        slime.materials = slimeMaterialWind;
                                        body.material = playerMaterial[3];
                                        /* playerMaterial.EnableKeyword("_CURRENT_ELEMENT_WIND");
                                        slimeMaterial.EnableKeyword("_CURRENT_ELEMENT_WIND");
                                        slimeEyesMaterial.EnableKeyword("_CURRENT_ELEMENT_WIND");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_EARTH");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_EARTH");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_EARTH");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_WATER");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_WATER");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_WATER");
                                 
                                        playerMaterial.DisableKeyword("_CURRENT_ELEMENT_NONE");
                                        slimeMaterial.DisableKeyword("_CURRENT_ELEMENT_NONE");
                                        slimeEyesMaterial.DisableKeyword("_CURRENT_ELEMENT_NONE");*/
                                        break;
                        }
                
                }
        }

}

